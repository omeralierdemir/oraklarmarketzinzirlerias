package _01.process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JTable;

public class DosyaRaporla {
	
	public void raporlama(JTable table) {
		
		// table_2 alacak arguman olarak
		
		try{
			Date today = new Date();
			SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy") ;
			String date = date_format.format(today) ;
			
			File file = new File("D:\\"+date+".txt") ;
			if(!file.exists()){
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile()) ;
			BufferedWriter bw = new BufferedWriter(fw) ;
			
			for(int i = 0 ; i<table.getRowCount() ; i++){
				for(int j = 0 ; j<table.getColumnCount() ; j++ ){
					
					 
					bw.write((Object)table.getModel().getValueAt(i, j)+" --> ");
			
				}
				
				bw.write("\n");
			}
			bw.close();
			fw.close();
			
			JOptionPane.showMessageDialog(null, "Dosya oluşturuldu.");
		}catch(Exception ex){	
			ex.printStackTrace();
		}
	}

}
