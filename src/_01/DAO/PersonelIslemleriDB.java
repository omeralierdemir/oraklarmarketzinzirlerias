package _01.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class PersonelIslemleriDB {

	Connection con;

	public PersonelIslemleriDB() {

		try {

			con = DriverManager.getConnection(
					"jdbc:mysql://localhost/oraklarmarket?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root", "omer");

		} catch (Exception e) {
			System.err.println(e);
		}

	}

	public void genelMudurEkleme(String tc, String isim, String parola, String kidem, String adres, String tel,
			String mail) {

		String query = "INSERT INTO oraklarmarket.genel_mudur(tc, isim, parola, kidem, adres, telefon, mail) VALUES('" + tc
				+ "', '" + isim + "', '" + parola + "', '" + kidem + "', '" + adres + "', '" + tel + "', '" + mail
				+ "')";

		try {

			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci");
			System.err.println(e.getMessage());
		}

	}

	public void genelMudurCikarma(String tc) {

		String query = "delete from oraklarmarket.genel_mudur where tc = '" + tc + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}

	public void yoneticiCikarma(String tc) {

		String query = "delete from oraklarmarket.yonetici where tc = '" + tc + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}

	public void mudurGuncelle(String tc, String isim, String parola, String kidem, String adres, String telefon,
			String mail) {

		String query = "update  oraklarmarket.genel_mudur set tc= '" + tc + "', isim='" + isim + "', parola='" + parola
				+ "', kidem='" + kidem + "', adres='" + adres + "', telefon='" + telefon + "', mail='" + mail
				+ "'  where tc = '" + tc + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}
	
	

	public void yoneticiGuncelle(String tc, String isim, String parola, String kidem, String adres, String telefon,
			String mail, String subeID) {

		String query = "update  oraklarmarket.yonetici set tc= '" + tc + "', isim='" + isim + "', parola='" + parola
				+ "', kidem='" + kidem + "', adres='" + adres + "', telefon='" + telefon + "', mail='" + mail
				+ "', subeID='" + subeID + "' where tc = '" + tc + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}
	public void calisanGuncelle(String tc, String isim, String parola, String bolum, String adres, String telefon,
			String mail, String subeID) {

		String query = "update  oraklarmarket.calisanlar set tc= '" + tc + "', isim='" + isim + "', parola='" + parola
				+ "', bolum='" + bolum + "', adres='" + adres + "', telefon='" + telefon + "', mail='" + mail
				+ "', subeID='" + subeID + "' where tc = '" + tc + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}

	public List<Object> mudurVerileri(String tc) {

		String query = "select * from oraklarmarket.genel_mudur where tc ='" + tc + "'";
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {

				a1.add(rs.getString("tc"));
				a1.add(rs.getString("isim"));
				a1.add(rs.getString("parola"));
				a1.add(rs.getString("kidem"));
				a1.add(rs.getString("adres"));
				a1.add(rs.getString("telefon"));
				a1.add(rs.getString("mail"));

			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!", 2);
		}
		return a1;

	}

	public List<Object> yoneticiVerileri(String tc) {

		String query = "select * from oraklarmarket.yonetici where tc ='" + tc + "'";
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {

				a1.add(rs.getString("tc"));
				a1.add(rs.getString("isim"));
				a1.add(rs.getString("parola"));
				a1.add(rs.getString("kidem"));
				a1.add(rs.getString("adres"));
				a1.add(rs.getString("telefon"));
				a1.add(rs.getString("mail"));
				a1.add(rs.getString("subeID"));
			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!", 2);
		}
		
		return a1;

	}

	public List<Object> calisanVerileri(String tc) {

		String query = "select * from oraklarmarket.calisanlar where tc ='" + tc + "'";
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {

				a1.add(rs.getString("tc"));
				a1.add(rs.getString("isim"));
				a1.add(rs.getString("parola"));
				a1.add(rs.getString("bolum"));
				a1.add(rs.getString("adres"));
				a1.add(rs.getString("telefon"));
				a1.add(rs.getString("mail"));
				a1.add(rs.getString("subeID"));
			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!", 2);
		}
		return a1;

	}

	public void calisanCikarma(String tc) {

		String query = "delete from oraklarmarket.calisanlar where tc = '" + tc + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}

	public void yoneticiEkleme(String tc, String isim, String parola, String kidem, String adres, String tel,
			String mail, String subeID) {

		String query = "INSERT INTO oraklarmarket.yonetici(tc, isim, parola, kidem, adres, telefon, mail,subeID) VALUES('" + tc
				+ "', '" + isim + "', '" + parola + "', '" + kidem + "', '" + adres + "', '" + tel + "', '" + mail
				+ "', '" + subeID + "')";

		try {

			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci");
			System.err.println(e.getMessage());
		}

	}

	public void calisanEkleme(String tc, String isim, String parola, String bolum, String adres, String tel,
			String mail, String subeID) {

		String query = "INSERT INTO oraklarmarket.calisanlar(tc, isim, parola, bolum, adres, telefon, mail, subeID) VALUES('" + tc
				+ "', '" + isim + "', '" + parola + "', '" + bolum + "', '" + adres + "', '" + tel + "', '" + mail
				+ "', '" + subeID + "')";

		try {

			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci");
			System.err.println(e.getMessage());
		}

	}

}
