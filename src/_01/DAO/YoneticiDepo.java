package _01.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class YoneticiDepo {

	Connection con;

	public YoneticiDepo() {

		try {

			con = DriverManager.getConnection(
					"jdbc:mysql://localhost/oraklarmarket?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root", "omer");

		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public List<Object> depoVerileri(String barkod) {

		String query = "select * from oraklarmarket.depo where barkodID ='" + barkod + "'";
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {

				a1.add(rs.getString("barkodID"));
				a1.add(rs.getString("urunAD"));
				a1.add(rs.getString("tur"));
				a1.add(rs.getString("marka"));
				a1.add(rs.getDouble("alis_fiyat"));
				a1.add(rs.getDouble("satis_fiyat"));
				a1.add(rs.getString("son_kullanma_tarihi"));
				a1.add(rs.getInt("stok_miktari"));
			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}
		return a1;

	}

	public void stokGuncelleme(String stokMiktari,String barkodID) {

		
		
		String query= "UPDATE oraklarmarket.depo SET stok_miktari ="+stokMiktari+" where barkodID='"+barkodID+"';" ;
		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}
	}

	public DefaultTableModel depoVerileri(DefaultTableModel model) {

		String query = "select * from oraklarmarket.depo";
		String[] sutun = { "Barkod ID", "Urun Ad�", "Tur", "Marka", "Alis Fiyati", "Satis Fiyati",
				"Son Kullanma Tarihi", "Stok Miktari" };
		model.setColumnIdentifiers(sutun);
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				a1.clear();

				a1.add(rs.getString("barkodID"));
				a1.add(rs.getString("urunAD"));
				a1.add(rs.getString("tur"));
				a1.add(rs.getString("marka"));
				a1.add(rs.getDouble("alis_fiyat"));
				a1.add(rs.getDouble("satis_fiyat"));
				a1.add(rs.getString("son_kullanma_tarihi"));
				a1.add(rs.getInt("stok_miktari"));
				model.addRow(a1.toArray());

			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!", 2);
		}
		return model;

	}
	public DefaultTableModel depoVerileri(DefaultTableModel model,String kosul,String kisit) {

		String query = "select * from oraklarmarket.depo where " + kosul + " = '" + kisit + "'" ;
		String[] sutun = { "Barkod ID", "Urun Ad�", "Tur", "Marka", "Alis Fiyati", "Satis Fiyati",
				"Son Kullanma Tarihi", "Stok Miktari" };
		model.setColumnIdentifiers(sutun);
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				a1.clear();

				a1.add(rs.getString("barkodID"));
				a1.add(rs.getString("urunAD"));
				a1.add(rs.getString("tur"));
				a1.add(rs.getString("marka"));
				a1.add(rs.getDouble("alis_fiyat"));
				a1.add(rs.getDouble("satis_fiyat"));
				a1.add(rs.getString("son_kullanma_tarihi"));
				a1.add(rs.getInt("stok_miktari"));
				model.addRow(a1.toArray());

			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!", 2);
		}
		return model;

	}


}
