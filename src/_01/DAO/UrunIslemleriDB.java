package _01.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UrunIslemleriDB {

	Connection con;

	public UrunIslemleriDB() {

		try {

			con = DriverManager.getConnection(
					"jdbc:mysql://localhost/oraklarmarket?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root", "omer");

		} catch (Exception e) {
			System.err.println(e);
		}

	}

	public void urunEkle(String barkod, String urunAd, String tur, String marka, Double alis_fiyat, Double satis_fiyat,
			String sonK_tarihi, Integer stok) {
		System.out.println("omerrrr");

		String query = "INSERT INTO oraklarmarket.depo(barkodID, urunAD, tur, marka, alis_fiyat, satis_fiyat, son_kullanma_tarih, stok_miktari) VALUES('"
				+ barkod + "', '" + urunAd + "', '" + tur + "', '" + marka + "'," + alis_fiyat + "," + satis_fiyat
				+ ",  '" + sonK_tarihi + "'," + stok + ")";
		try {

			Statement st = con.createStatement();
			st.executeUpdate(query);
		} catch (Exception e) {

			System.out.println("buraya bak haci");
			System.err.println(e.getMessage());
		}

	}

	public void urunCikarma(String barkod) {

		String query = "delete from oraklarmarket.depo where barkodID = '" + barkod + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}

	public List<Object> tabloVerileri(String barkod) {

		String query = "select * from oraklarmarket.depo where barkodID ='" + barkod + "'";
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {

				a1.add(rs.getString("barkodID"));
				a1.add(rs.getString("urunAD"));
				a1.add(rs.getString("tur"));
				a1.add(rs.getString("marka"));
				a1.add(rs.getDouble("alis_fiyat"));
				a1.add(rs.getDouble("satis_fiyat"));
				a1.add(rs.getString("son_kullanma_tarihi"));
				a1.add(rs.getInt("stok_miktari"));
			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}
		return a1;

	}

//	public static void main(String[] args) {
//		
//		try {
//			Connection conn = DriverManager.getConnection(
//					"jdbc:mysql://localhost/bakkal?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
//					"root", "omer");
//			System.out.println("Ba�lant� Sa�land");
//			String query = "Select * From depo";
//			Statement stmt = conn.createStatement();
//			ResultSet rs = stmt.executeQuery(query);
//
//			while (rs.next()) {
//				System.out.println("ID:" + rs.getString("barkod") + " ADI:" + rs.getString("urunAd"));
//			}
//
//		} catch (Exception e) {
//			System.err.println(e);
//		}
//	}

}
