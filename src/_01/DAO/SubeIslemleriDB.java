package _01.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class SubeIslemleriDB {

	Connection con;

	public SubeIslemleriDB() {

		try {

			con = DriverManager.getConnection(
					"jdbc:mysql://localhost/oraklarmarket?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root", "omer");

		} catch (Exception e) {
			System.err.println(e);
		}

	}

	public void subeEkleme(String subeID, String isim, String adres, String tel, String mail, String kidem) {

		String query = "INSERT INTO oraklarmarket.sube(subeID, isim, adres, telefon, mail, kidem) VALUES(" + subeID + ", '" + isim
				+ "', '" + adres + "', '" + tel + "', '" + mail + "', '" + kidem+ "')";

		try {

			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci");
			System.err.println(e.getMessage());
		}

	}

	public void subeSilme(String subeID) {

		String query = "delete from oraklarmarket.sube where subeID = '" + subeID + "'";

		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}
	
	public void subeGuncelle(String subeID, String isim, String adres, String tel, String mail, String kidem) {


		
		String query = "update  oraklarmarket.sube set subeID= '" + subeID + "', isim='" + isim + "', adres='" + adres
				+ "', telefon='" + tel + "', mail='" + mail + "', kidem='" + kidem +  "'  where subeID = " + subeID ;
		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
		}

	}

	public DefaultTableModel subeVerileri( DefaultTableModel model) {

		String query = "select * from oraklarmarket.sube";
		String[] sutun = { "Sube Numaras�", "Sube �smi", "Adres", "Telefon", "E-Mail", "Sube Kidem" };
		model.setColumnIdentifiers(sutun);
		List<Object> a1 = new ArrayList<>();

		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				a1.clear();
				a1.add(rs.getString("subeID"));
				a1.add(rs.getString("isim"));
				a1.add(rs.getString("adres"));
				a1.add(rs.getString("telefon"));
				a1.add(rs.getString("mail"));
				a1.add(rs.getString("kidem"));

				model.addRow(a1.toArray());

			}

		} catch (Exception e) {

			System.out.println("buraya bak haci delete");
			System.err.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!", 2);
		}
		return model;

	}

}
