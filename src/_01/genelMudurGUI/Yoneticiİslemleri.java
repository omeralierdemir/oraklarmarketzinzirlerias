package _01.genelMudurGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import _01.DAO.YoneticiDepo;
import _01.DAO.YoneticiRaporlamaDB;
import _01.process.DosyaRaporla;

public class Yonetici�slemleri extends JFrame {

	protected static final int MY_DEFAULT_VALUE = 0;
	private JPanel contentPane;
	JLabel hosgeldiniz = new JLabel("New label");
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTable table;
	private JTextField txtField_BarkodNo = new JTextField();;
	private JTextField txtField_TalepAdedi;
	private JTextField textField_urunTuru;
	private JTextField textField_urunAdi = new JTextField();
	private JTable table_1;
	private JTable table_2;
	private DosyaRaporla d1 = new DosyaRaporla();
	private YoneticiRaporlamaDB raporDB = new YoneticiRaporlamaDB();
	private YoneticiDepo yoneticiDepo = new YoneticiDepo();
	private DefaultTableModel model_1 = new DefaultTableModel();
	private DefaultTableModel model_2 = new DefaultTableModel();
	private DefaultTableModel model_3 = new DefaultTableModel();

	boolean rdBtnBarkodIleBoolean = false;
	boolean rdBtnUrunAdiIleBoolean = false;
	boolean rdBtnUrunTuruIleBoolean = false;
	private JTextField textField_Ara;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Yonetici�slemleri frame = new Yonetici�slemleri();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Yonetici�slemleri() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 766, 570);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(Color.WHITE);

		contentPane.add(hosgeldiniz);
		hosgeldiniz.setFont(new Font("Palatino Linotype", Font.BOLD, 18));
		hosgeldiniz.setBounds(10, 11, 247, 51);
		// hosgeldiniz.setText("Ho�geldiniz Say�n "+getUserName); heralde isim verecen

		setVisible(true);
		setTitle("Y�netici Men�s�");
		setLocationRelativeTo(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(null);
		tabbedPane.setBounds(10, 62, 740, 459);
		tabbedPane.setBackground(Color.WHITE);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("�r�n Talebi", null, panel, null);
		tabbedPane.setEnabledAt(0, true);

		tabbedPane.setFont(new Font("Palatino Linotype", Font.BOLD, 11));

		panel.setBackground(Color.WHITE);
		panel.setLayout(null);

		JSeparator separator = new JSeparator();
		separator.setBounds(481, 11, 2, 409);
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setForeground(Color.GRAY);
		separator.setBackground(Color.GRAY);
		panel.add(separator);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setBounds(10, 11, 461, 409);
		scrollPane.getViewport().setBackground(Color.white);

		panel.add(scrollPane);

		table = new JTable();

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				int i = table.getSelectedRow();
				TableModel model = table.getModel();
				txtField_BarkodNo.setText(model.getValueAt(i, 0).toString());
				textField_urunTuru.setText(model.getValueAt(i, 2).toString());
				textField_urunAdi.setText(model.getValueAt(i, 1).toString());
				txtField_TalepAdedi.setText(" ");

			}
		});
		table.setFont(new Font("Palatino Linotype", Font.PLAIN, 11));

		scrollPane.setViewportView(table);

//		
//		try{
		model_1 = yoneticiDepo.depoVerileri(model_1);
		table.setModel(model_1);

		JLabel lblNewLabel = new JLabel("Barkod Numaras\u0131:");
		lblNewLabel.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		lblNewLabel.setBounds(493, 11, 95, 14);
		panel.add(lblNewLabel);

		txtField_BarkodNo.setEditable(false);
		txtField_BarkodNo.setBounds(589, 11, 136, 20);
		panel.add(txtField_BarkodNo);
		txtField_BarkodNo.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Talep Edilen Adet:");
		lblNewLabel_1.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		lblNewLabel_1.setBounds(493, 90, 95, 14);
		panel.add(lblNewLabel_1);

		txtField_TalepAdedi = new JTextField();
		txtField_TalepAdedi.setToolTipText("Talep edilen adedi giriniz.\r\n");
		txtField_TalepAdedi.setBounds(589, 90, 136, 20);
		panel.add(txtField_TalepAdedi);

		JButton btnTalepEt = new JButton("Talep Et");
		btnTalepEt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int i = table.getSelectedRow();
				System.out.println(model_1.getValueAt(i, 7));
				int selectedProductNumber = (int) model_1.getValueAt(i, 7); // Se�ilen �r�n�n adedini al�r.
				String selectedProductBarkodNo = (String) model_1.getValueAt(i, 0); // Se�ilen �r�n�n BarkodNumaras�n�
																					// al�r.
				String getTalepAdedi = txtField_TalepAdedi.getText().trim(); // �r�n Adedi textBox'�ndan girilen �r�n
																				// adedini al�r.
				int textboxSelectedProductCount = Integer.parseInt(getTalepAdedi); // �r�n Adedi textBox'�ndan girilen
																					// �r�n adedini al�r.

				System.out.println(textboxSelectedProductCount);
				Integer urunYeterlimi = selectedProductNumber - textboxSelectedProductCount;

				if (urunYeterlimi >= 0) {
					String queryUpdateValue = "UPDATE depo SET UrunAdedi ='" + urunYeterlimi
							+ "' where BarkodNumarasi='" + selectedProductBarkodNo + "';";

					yoneticiDepo.stokGuncelleme(urunYeterlimi.toString(), selectedProductBarkodNo);
					System.out.println(queryUpdateValue);

					model_1.setRowCount(0);
					model_1 = yoneticiDepo.depoVerileri(model_1);
					table.setModel(model_1);
					Date d1 = new Date();
					System.out.println(d1);
					String talepRaporu = selectedProductBarkodNo + " Barkod Numarali Urunden "
							+ textboxSelectedProductCount + " adet talep edildi.";
//					raporToDb(d1, talepRaporu);
					raporDB.raporEkle(d1.toString(), talepRaporu);

					model_2.setRowCount(0);
					model_2 = yoneticiDepo.depoVerileri(model_2);
					table_1.setModel(model_2);

					model_3.setRowCount(0);
					model_3 = yoneticiDepo.depoVerileri(model_3);
					table_2.setModel(model_3);

				} else {
					JOptionPane.showMessageDialog(null, "Yeterli stok yok ! ��lem yap�lamad�. ");

				}
			}
		});
		btnTalepEt.setBackground(Color.BLACK);
		btnTalepEt.setForeground(Color.WHITE);
		btnTalepEt.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		btnTalepEt.setBounds(636, 142, 89, 23);
		panel.add(btnTalepEt);

		JLabel lblNewLabel_2 = new JLabel("\u00DCr\u00FCn T\u00FCr\u00FC:\r\n");
		lblNewLabel_2.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		lblNewLabel_2.setBounds(493, 40, 95, 14);
		panel.add(lblNewLabel_2);

		textField_urunTuru = new JTextField();
		textField_urunTuru.setEditable(false);
		textField_urunTuru.setBounds(589, 40, 136, 20);
		panel.add(textField_urunTuru);
		textField_urunTuru.setColumns(10);

		JLabel lblrnAd = new JLabel("\u00DCr\u00FCn Ad\u0131:\r\n");
		lblrnAd.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		lblrnAd.setBounds(493, 65, 95, 14);
		panel.add(lblrnAd);

		textField_urunAdi.setEditable(false);
		textField_urunAdi.setBounds(589, 65, 136, 20);
		panel.add(textField_urunAdi);
		textField_urunAdi.setColumns(10);

		/*
		 * JButton replayBtn = new JButton(""); replayBtn.setLocation(509, 130);
		 * replayBtn.setSize(44, 39); replayBtn.setBackground(Color.WHITE); Image photo
		 * = new ImageIcon(this.getClass().getResource("/indir.png")).getImage();
		 * replayBtn.setIcon(new ImageIcon(photo));
		 * replayBtn.setToolTipText("Tabloyu yenilemek i�in t�klay�n�z");
		 * replayBtn.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent e) { model.setRowCount(0); showInTable() ; } });
		 * //replayBtn.setBounds(516, 134, 40, 36); panel.add(replayBtn);
		 */

//			con.close();
//		}
//		
//		catch(Exception e){
//			e.printStackTrace();
//		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			Connection con =  DriverManager.getConnection("jdbc:mysql://localhost:3306/market_otomasyon", "root", "") ;
//			String query = "select * from depo " ;
//			PreparedStatement pst = con.prepareStatement(query) ;
//			ResultSet rs = pst.executeQuery() ;
//			table_1.setModel(DbUtils.resultSetToTableModel(rs));
//			
		

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("�r�n", null, panel_1, null);
		tabbedPane.setEnabledAt(1, true);
		panel_1.setBackground(Color.WHITE);
		panel_1.setLayout(null);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(481, 11, 2, 409);
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setForeground(Color.GRAY);
		separator_1.setBackground(Color.GRAY);
		panel_1.add(separator_1);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(null);
		scrollPane_1.setBounds(10, 11, 461, 409);
		scrollPane_1.getViewport().setBackground(Color.white);
		panel_1.add(scrollPane_1);

		table_1 = new JTable();
		table_1.setFont(new Font("Palatino Linotype", Font.PLAIN, 11));
		
		model_2 = yoneticiDepo.depoVerileri(model_2);
		
		table_1.setBackground(Color.WHITE);
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				int i = table_1.getSelectedRow();
				TableModel model = table_1.getModel();

			}
		});
		scrollPane_1.setViewportView(table_1);
		table_1.setModel(model_2);

		JRadioButton rdbtnBarkodNumarasIle = new JRadioButton("Barkod Numaras\u0131 ile Ara");
		rdbtnBarkodNumarasIle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				rdBtnBarkodIleBoolean = true;
				rdBtnUrunAdiIleBoolean = false;
				rdBtnUrunTuruIleBoolean = false;
			}
		});
		rdbtnBarkodNumarasIle.setFont(new Font("Palatino Linotype", Font.BOLD, 12));
		rdbtnBarkodNumarasIle.setBackground(Color.WHITE);
		buttonGroup.add(rdbtnBarkodNumarasIle);
		rdbtnBarkodNumarasIle.setBounds(506, 11, 191, 23);
		panel_1.add(rdbtnBarkodNumarasIle);

		JRadioButton rdbtnNewUrunAdiIle = new JRadioButton("\u00DCr\u00FCn Ad\u0131 ile Ara");
		rdbtnNewUrunAdiIle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				rdBtnUrunAdiIleBoolean = true;
				rdBtnUrunTuruIleBoolean = false;
				rdBtnBarkodIleBoolean = false;
			}
		});
		rdbtnNewUrunAdiIle.setFont(new Font("Palatino Linotype", Font.BOLD, 12));
		rdbtnNewUrunAdiIle.setBackground(Color.WHITE);
		buttonGroup.add(rdbtnNewUrunAdiIle);
		rdbtnNewUrunAdiIle.setBounds(505, 37, 192, 23);
		panel_1.add(rdbtnNewUrunAdiIle);

		JRadioButton rdbtnUrunTuruIle = new JRadioButton("\u00DCr\u00FCn T\u00FCr\u00FC ile Ara");
		rdbtnUrunTuruIle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				rdBtnUrunTuruIleBoolean = true;
				rdBtnBarkodIleBoolean = false;
				rdBtnUrunAdiIleBoolean = false;
			}
		});
		rdbtnUrunTuruIle.setFont(new Font("Palatino Linotype", Font.BOLD, 12));
		rdbtnUrunTuruIle.setBackground(Color.WHITE);
		buttonGroup.add(rdbtnUrunTuruIle);
		rdbtnUrunTuruIle.setBounds(506, 63, 191, 23);
		panel_1.add(rdbtnUrunTuruIle);

		textField_Ara = new JTextField();
		textField_Ara.setBounds(506, 138, 191, 20);
		panel_1.add(textField_Ara);
		textField_Ara.setColumns(10);

		JButton btn_Ara = new JButton("Ara");
		btn_Ara.setToolTipText("\u00DCr\u00FCn Aramak \u0130\u00E7in T\u0131klay\u0131n\u0131z");
		btn_Ara.setForeground(Color.BLACK);
		btn_Ara.setBackground(Color.BLACK);

//			Image photo2 = new ImageIcon(this.getClass().getResource("/images.jpg")).getImage();
//			btn_Ara.setIcon(new ImageIcon(photo2));
		btn_Ara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdBtnBarkodIleBoolean) {

					model_2.setRowCount(0);
					model_2 = yoneticiDepo.depoVerileri(model_2, "barkodID", textField_Ara.getText());
					table_1.setModel(model_2);
					textField_Ara.setText("");
				}
				if (rdBtnUrunTuruIleBoolean) {

					model_2.setRowCount(0);
					model_2 = yoneticiDepo.depoVerileri(model_2, "tur", textField_Ara.getText());
					table_1.setModel(model_2);
					textField_Ara.setText("");

				}

				if (rdBtnUrunAdiIleBoolean) {
					model_2.setRowCount(0);
					model_2 = yoneticiDepo.depoVerileri(model_2, "urunAD", textField_Ara.getText());
					table_1.setModel(model_2);
					textField_Ara.setText("");

				}

			}
		});
		btn_Ara.setBounds(573, 177, 58, 58);
		panel_1.add(btn_Ara);

		JButton btnNewButton = new JButton("T\u00FCm \u00DCr\u00FCnleri G\u00F6r");
		btnNewButton.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(Color.BLACK);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				model_2.setRowCount(0);
				model_2 = yoneticiDepo.depoVerileri(model_2);
				table_1.setModel(model_2);

			}
		});
		btnNewButton.setBounds(506, 93, 191, 23);
		panel_1.add(btnNewButton);

		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Raporlama", null, panel_2, null);
		panel_2.setBackground(Color.WHITE);
		panel_2.setLayout(null);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(574, 11, 2, 409);
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setForeground(Color.GRAY);
		separator_2.setBackground(Color.GRAY);
		panel_2.add(separator_2);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setViewportBorder(null);
		scrollPane_2.setBounds(10, 11, 554, 409);
		scrollPane_2.getViewport().setBackground(Color.white);
		panel_2.add(scrollPane_2);

		table_2 = new JTable();
		table_2.setFont(new Font("Palatino Linotype", Font.PLAIN, 11));
		table_2.setBackground(Color.WHITE);
		scrollPane_2.setViewportView(table_2);

		JButton btnNewButton_1 = new JButton("Dosya Olu\u015Ftur");
		btnNewButton_1.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setBackground(Color.BLACK);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				d1.raporlama(table_2);

			}
		});
		btnNewButton_1.setBounds(586, 11, 139, 23);
		panel_2.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("Tabloyu Yenile");
		btnNewButton_2.setFont(new Font("Palatino Linotype", Font.BOLD, 11));
		btnNewButton_2.setForeground(Color.WHITE);
		btnNewButton_2.setBackground(Color.BLACK);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				model_3.setRowCount(0);
				model_3 = yoneticiDepo.depoVerileri(model_3);
				table_2.setModel(model_3);
			}
		});
		btnNewButton_2.setBounds(586, 45, 139, 23);
		panel_2.add(btnNewButton_2);
//			DefaultTableModel model = (DefaultTableModel)table_1.getModel() ;
//			
//			tumTableRaporGor();
//			
//			con.close();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

//
//		public ArrayList<Urun> getUrunList(){
//			
//			ArrayList<Urun> urunList = new ArrayList<Urun>() ;
//			Connection connection = getConnection();
//			String query= "select * from depo" ;
//			java.sql.Statement st ;
//			ResultSet rs ;
//			
//			try{
//				st = connection.createStatement();
//				rs = st.executeQuery(query);
//				Urun urun ;
//				while(rs.next()){
//					
//					urun = new Urun(rs.getInt("BarkodNumarasi"),rs.getString("UrunTuru"),rs.getString("UrunAdi"),rs.getInt("UrunAdedi")) ;
//					urunList.add(urun);
//					
//				}
//			}catch(Exception e){
//				
//			}
//		return urunList ;
//		}
//		
//		public void showInTable(){
//			
//			ArrayList<Urun> list = getUrunList() ;
//			DefaultTableModel model = (DefaultTableModel)table.getModel();
//			DefaultTableModel model2 = (DefaultTableModel)table_1.getModel();
//			Object[] row = new Object[4] ;
//			
//			for(int i = 0 ; i < list.size(); i++){
//				
//				row[0] = list.get(i).getBarkodNo() ;
//				row[1] = list.get(i).getUrunTuru() ;
//				row[2] = list.get(i).getUrunAdi() ;
//				row[3] = list.get(i).getUrunAdedi() ;
//				
//				model.addRow(row);
//				model2.addRow(row);	
//			}
//		}
//		public void tumTabloyuGor(){
//			
//			String query = "select * from depo;" ;
//			try{
//			Class.forName("com.mysql.jdbc.Driver");
//			Connection con =  DriverManager.getConnection("jdbc:mysql://localhost:3306/market_otomasyon", "root", "") ;
//			PreparedStatement pst = con.prepareStatement(query) ;
//			ResultSet rs = pst.executeQuery() ;
//			
//			table_1.setModel(DbUtils.resultSetToTableModel(rs));
//			con.close();
//			}catch(Exception i){
//				i.printStackTrace();
//			}	
//		}
//		
//		public void tumTableBirGor(){
//			
//			String query = "select * from depo;" ;
//			try{
//			Class.forName("com.mysql.jdbc.Driver");
//			Connection con =  DriverManager.getConnection("jdbc:mysql://localhost:3306/market_otomasyon", "root", "") ;
//			PreparedStatement pst = con.prepareStatement(query) ;
//			ResultSet rs = pst.executeQuery() ;
//			
//			table.setModel(DbUtils.resultSetToTableModel(rs));
//			con.close();
//			}catch(Exception i){
//				i.printStackTrace();
//			}
//			
//		}
//		
//		public void tumTableRaporGor(){
//			String query = "select * from rapor" ;
//			Connection con = getConnection() ;
//			try{
//				PreparedStatement pst = con.prepareStatement(query) ;
//				ResultSet rs = pst.executeQuery() ;
//				table_2.setModel(DbUtils.resultSetToTableModel(rs));
//				con.close();
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//			
//		}
//		
//		public void barkodNoIleAra(){
//			
//			String query = "select * from depo where BarkodNumarasi='"+textField_Ara.getText()+"';" ;
//			try{
//			Class.forName("com.mysql.jdbc.Driver");
//			Connection con =  DriverManager.getConnection("jdbc:mysql://localhost:3306/market_otomasyon", "root", "") ;
//			PreparedStatement pst = con.prepareStatement(query) ;
//			ResultSet rs = pst.executeQuery() ;
//			table_1.setModel(DbUtils.resultSetToTableModel(rs));
//			con.close();
//			
//			
//			}catch(Exception i){
//				i.printStackTrace();
//			}
//		}
//		
//		public void urunTuruIleAra(){
//			Connection con = getConnection() ;
//			PreparedStatement pst ;
//			ResultSet rs ;
//			String query = "select * from depo where UrunTuru='"+textField_Ara.getText()+"';" ;
//			try{
//				pst = con.prepareStatement(query) ;
//				rs = pst.executeQuery() ;
//				table_1.setModel(DbUtils.resultSetToTableModel(rs));
//				con.close(); 
//			}
//			catch(Exception e){
//				e.printStackTrace();
//			}
//			
//		}
//		
//		public void raporToDb(Date date,String rapor){
//			
//			String query = "INSERT INTO rapor ( date ,rapor ) VALUES ( '"+date+"' , '" + rapor+"' ) ;" ;
//			queryExecute(query);
//		}
//		
//		public void urunAdilIleAra(){
//			Connection con = getConnection() ;
//			PreparedStatement pst ;
//			ResultSet rs ;
//			String query =  "select * from depo where UrunAdi='"+textField_Ara.getText()+"';" ;
//			try {
//				
//			pst = con.prepareStatement(query) ;
//			rs = pst.executeQuery() ;
//			table_1.setModel(DbUtils.resultSetToTableModel(rs));
//			con.close();
//			} 
//			
//			catch(Exception e){ 
//		
//				e.printStackTrace();
//			}
//		
//		
//		}
//	}

}
