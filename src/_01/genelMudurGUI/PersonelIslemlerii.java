package _01.genelMudurGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import _01.DAO.PersonelIslemleriDB;

import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PersonelIslemlerii extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldM1;
	private JTextField textFieldM2;
	private JTextField textFieldM3;
	private JTextField textFieldM4;
	private JTextField textFieldM5;
	private JTextField textFieldM6;
	private JTextField textFieldM7;
	private JRadioButton radioButton;
	private JRadioButton radioButton_1;
	PersonelIslemleriDB personelEkle = new PersonelIslemleriDB();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PersonelIslemlerii frame = new PersonelIslemlerii();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PersonelIslemlerii() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1400, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JRadioButton rdbtnNewRadioButton = new JRadioButton(
				"\u00DCst D\u00FCzey Y\u00F6netici Eklemek i\u00E7in T\u0131klay\u0131n\u0131z..");
		rdbtnNewRadioButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtnNewRadioButton.setBounds(30, 33, 566, 33);
		contentPane.add(rdbtnNewRadioButton);

		JPanel panel = new JPanel();
		panel.setBounds(40, 80, 1160, 205);
		contentPane.add(panel);
		panel.setLayout(null);

		textFieldM1 = new JTextField();
		textFieldM1.setBounds(32, 13, 221, 45);
		panel.add(textFieldM1);
		textFieldM1.setColumns(10);

		textFieldM2 = new JTextField();
		textFieldM2.setColumns(10);
		textFieldM2.setBounds(328, 13, 221, 45);
		panel.add(textFieldM2);

		textFieldM3 = new JTextField();
		textFieldM3.setColumns(10);
		textFieldM3.setBounds(622, 13, 221, 45);
		panel.add(textFieldM3);

		textFieldM4 = new JTextField();
		textFieldM4.setColumns(10);
		textFieldM4.setBounds(897, 13, 221, 45);
		panel.add(textFieldM4);

		textFieldM5 = new JTextField();
		textFieldM5.setColumns(10);
		textFieldM5.setBounds(32, 110, 221, 45);
		panel.add(textFieldM5);

		textFieldM6 = new JTextField();
		textFieldM6.setColumns(10);
		textFieldM6.setBounds(328, 110, 221, 45);
		panel.add(textFieldM6);

		textFieldM7 = new JTextField();
		textFieldM7.setColumns(10);
		textFieldM7.setBounds(622, 110, 221, 45);
		panel.add(textFieldM7);

		JButton btnNewButton = new JButton("Ekle");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				personelEkle.genelMudurEkleme(textFieldM1.getText(), textFieldM2.getText(), textFieldM3.getText(),
						textFieldM4.getText(), textFieldM5.getText(), textFieldM6.getText(), textFieldM7.getText());

			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.setBounds(897, 109, 221, 45);
		panel.add(btnNewButton);

		radioButton = new JRadioButton(
				"\u00DCst D\u00FCzey Y\u00F6netici Eklemek i\u00E7in T\u0131klay\u0131n\u0131z..");
		radioButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		radioButton.setBounds(30, 347, 566, 33);
		contentPane.add(radioButton);

		radioButton_1 = new JRadioButton(
				"\u00DCst D\u00FCzey Y\u00F6netici Eklemek i\u00E7in T\u0131klay\u0131n\u0131z..");
		radioButton_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		radioButton_1.setBounds(30, 557, 566, 33);
		contentPane.add(radioButton_1);
	}
}
