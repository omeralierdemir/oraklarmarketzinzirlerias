package _01.genelMudurGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class GMIslemleri extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GMIslemleri frame = new GMIslemleri();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GMIslemleri() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 860, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 842, 553);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("\u00DCr\u00FCn \u0130\u015Flemleri");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(315, 59, 210, 61);
		panel.add(btnNewButton);
		
		JButton btnDepostokIlemleri = new JButton("Depo-Stok \u0130\u015Flemleri");
		btnDepostokIlemleri.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDepostokIlemleri.setBounds(315, 146, 210, 61);
		panel.add(btnDepostokIlemleri);
		
		JButton btnPersonelIlemleri = new JButton("Personel \u0130\u015Flemleri");
		btnPersonelIlemleri.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnPersonelIlemleri.setBounds(315, 237, 210, 61);
		panel.add(btnPersonelIlemleri);
		
		JButton btnubeIlemleri = new JButton("\u015Eube \u0130\u015Flemleri");
		btnubeIlemleri.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnubeIlemleri.setBounds(315, 331, 210, 61);
		panel.add(btnubeIlemleri);
		
		JButton btnToptancIlemleri = new JButton("Toptanc\u0131 \u0130\u015Flemleri");
		btnToptancIlemleri.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnToptancIlemleri.setBounds(315, 428, 210, 61);
		panel.add(btnToptancIlemleri);
	}
}
