package _01.genelMudurGUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import _01.DAO.UrunIslemleriDB;

import javax.swing.ButtonGroup;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class UrunIslemleri extends JFrame {

	private JPanel contentPane;
	private JTextField txtOmer;
	private JTextField txtrnAd;
	private JTextField txtTr;
	private JTextField txtMarka;
	private JTextField txtAlFiyat;
	private JTextField txtSatFiyat;
	private JTextField txtSonKullanmaTarihi;
	private JTextField txtStokMiktar;
	private JButton btnNewButton;
	private JPanel panel_2 = new JPanel();
	private JTextField textField_7;
	private JButton btnNewButton_1;
	private JRadioButton rdbGuncelleme;
	JPanel panel_1 = new JPanel();
	JPanel panel_3 = new JPanel();

	JRadioButton rdbCikarma = new JRadioButton(
			"\u00DCr\u00FCn \u00C7\u0131karmak \u0130\u00E7in T\u0131klay\u0131n\u0131z..");

	JRadioButton rdbListeleme = new JRadioButton(
			"\u00DCr\u00FCn B\u0130lgisini G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");

	UrunIslemleriDB urunDB = new UrunIslemleriDB();

	private JTextField gField1;
	private JTextField gField3;
	private JTextField gField4;
	private JTextField gField5;
	private JTextField gField6;
	private JTextField gField7;
	private JTextField gField8;
	private JButton button;
	private JTextField gField2;
	private JTextField textField_1;
	private JButton btnrnAra;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UrunIslemleri frame = new UrunIslemleri();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UrunIslemleri() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1300, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1382, 953);
		contentPane.add(panel);
		panel.setLayout(null);

		JRadioButton rdbEkle = new JRadioButton(
				"Yeni \u00DCr\u00FCn Eklemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		rdbEkle.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_1.setBounds(25, 74, 1232, 219);

				rdbCikarma.setBounds(25, 302, 405, 32);
				rdbGuncelleme.setBounds(25, 344, 405, 32);
				rdbListeleme.setBounds(25, 388, 405, 32);

				panel_1.setVisible(true);
				panel_2.setVisible(false);
				panel_3.setVisible(false);
			}
		});

		buttonGroup.add(rdbEkle);
		rdbEkle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbEkle.setBounds(25, 19, 405, 32);
		panel.add(rdbEkle);

		buttonGroup.add(rdbCikarma);
		rdbCikarma.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_2.setBounds(25, 120, 1232, 219);
				rdbCikarma.setBounds(25, 76, 405, 32);
				rdbGuncelleme.setBounds(25, 190, 405, 32);
				rdbListeleme.setBounds(25, 234, 405, 32);

				panel_2.setVisible(true);
				panel_1.setVisible(false);
				panel_3.setVisible(false);

			}
		});
		rdbCikarma.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbCikarma.setBounds(25, 70, 405, 32);
		panel.add(rdbCikarma);

		rdbGuncelleme = new JRadioButton(
				"\u00DCr\u00FCn B\u0130lgisini G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		rdbGuncelleme.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				rdbCikarma.setBounds(25, 76, 405, 32);
				rdbGuncelleme.setBounds(25, 130, 405, 32);
				rdbListeleme.setBounds(25, 480, 405, 32);

				panel_3.setBounds(25, 154, 1232, 310);
				panel_3.setVisible(true);
				panel_2.setVisible(false);
				panel_1.setVisible(false);
			}
		});
		buttonGroup.add(rdbGuncelleme);
		rdbGuncelleme.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbGuncelleme.setBounds(25, 120, 405, 32);
		panel.add(rdbGuncelleme);
		buttonGroup.add(rdbListeleme);

		rdbListeleme.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbListeleme.setBounds(25, 170, 405, 32);
		panel.add(rdbListeleme);

		panel_1.setBounds(25, 60, 1232, 219);
		panel.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setVisible(false);
		

		txtOmer = new JTextField();
		txtOmer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtOmer.getText().equals("Baknot ID")) {

					txtOmer.setText("");
					txtOmer.setForeground(Color.BLACK);
					txtOmer.setEditable(true);

				}

			}

			@Override
			public void mouseExited(MouseEvent e) {

				if (txtOmer.getText().equals("")) {

					txtOmer.setForeground(Color.LIGHT_GRAY);
					txtOmer.setText("Baknot ID");
					txtOmer.setEditable(false);
				}

			}
		});

		txtOmer.setText("Baknot ID");

		txtOmer.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtOmer.setForeground(Color.LIGHT_GRAY);
		txtOmer.setBounds(35, 23, 221, 45);
		panel_1.add(txtOmer);
		txtOmer.setColumns(10);

		txtrnAd = new JTextField();
		txtrnAd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtrnAd.getText().equals("\u00DCr\u00FCn Ad\u0131")) {

					txtrnAd.setText("");
					txtrnAd.setForeground(Color.BLACK);
					txtrnAd.setEditable(true);

				}

			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (txtrnAd.getText().equals("")) {

					txtrnAd.setForeground(Color.LIGHT_GRAY);
					txtrnAd.setText("\u00DCr\u00FCn Ad\u0131");
					txtrnAd.setEditable(false);

				}
			}
		});
		txtrnAd.setForeground(Color.LIGHT_GRAY);
		txtrnAd.setText("\u00DCr\u00FCn Ad\u0131");
		txtrnAd.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtrnAd.setColumns(10);
		txtrnAd.setBounds(341, 23, 221, 45);
		panel_1.add(txtrnAd);

		txtTr = new JTextField();
		txtTr.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtTr.getText().equals("T\u00FCr")) {

					txtTr.setText("");
					txtTr.setForeground(Color.BLACK);
					txtTr.setEditable(true);

				}

			}

			@Override
			public void mouseExited(MouseEvent e) {

				if (txtTr.getText().equals("")) {

					txtTr.setForeground(Color.LIGHT_GRAY);
					txtTr.setText("T\u00FCr");
					txtTr.setEditable(false);
				}
			}
		});

		txtTr.setText("T\u00FCr");
		txtTr.setForeground(Color.LIGHT_GRAY);
		txtTr.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTr.setColumns(10);
		txtTr.setBounds(648, 23, 221, 45);
		panel_1.add(txtTr);

		txtMarka = new JTextField();
		txtMarka.setForeground(Color.LIGHT_GRAY);
		txtMarka.setText("Marka");
		txtMarka.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtMarka.setColumns(10);
		txtMarka.setBounds(961, 23, 221, 45);
		panel_1.add(txtMarka);

		txtAlFiyat = new JTextField();
		txtAlFiyat.setToolTipText("(birim fiyat\u0131)");
		txtAlFiyat.setText("Al\u0131\u015F Fiyat\u0131");
		txtAlFiyat.setForeground(Color.LIGHT_GRAY);
		txtAlFiyat.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAlFiyat.setColumns(10);
		txtAlFiyat.setBounds(35, 94, 221, 45);
		panel_1.add(txtAlFiyat);

		txtSatFiyat = new JTextField();
		txtSatFiyat.setText("Sat\u0131\u015F Fiyat\u0131");
		txtSatFiyat.setToolTipText("(birim fiyat\u0131)");
		txtSatFiyat.setForeground(Color.LIGHT_GRAY);
		txtSatFiyat.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtSatFiyat.setColumns(10);
		txtSatFiyat.setBounds(341, 94, 221, 45);
		panel_1.add(txtSatFiyat);

		txtSonKullanmaTarihi = new JTextField();
		txtSonKullanmaTarihi.setText("Son Kullanma Tarihi");
		txtSonKullanmaTarihi.setForeground(Color.LIGHT_GRAY);
		txtSonKullanmaTarihi.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtSonKullanmaTarihi.setColumns(10);
		txtSonKullanmaTarihi.setBounds(648, 94, 221, 45);
		panel_1.add(txtSonKullanmaTarihi);

		txtStokMiktar = new JTextField();
		txtStokMiktar.setToolTipText("Doldurulmas\u0131 Zorunlu De\u011Fildir");
		txtStokMiktar.setText("Stok Miktar\u0131");
		txtStokMiktar.setForeground(Color.LIGHT_GRAY);
		txtStokMiktar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtStokMiktar.setColumns(10);
		txtStokMiktar.setBounds(961, 94, 221, 45);
		panel_1.add(txtStokMiktar);

		btnNewButton = new JButton("\u00DCr\u00FCn Ekle");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				urunDB.urunEkle(txtOmer.getText(), txtrnAd.getText(), txtTr.getText(), txtMarka.getText(),
						Double.parseDouble(txtAlFiyat.getText()), Double.parseDouble(txtSatFiyat.getText()),
						txtSonKullanmaTarihi.getText(), Integer.parseInt(txtStokMiktar.getText()));

			}
		});
		btnNewButton.setBounds(961, 161, 221, 45);
		panel_1.add(btnNewButton);

		
		panel_2.setBounds(25, 338, 1232, 76);
		panel.add(panel_2);
		panel_2.setLayout(null);
		panel_2.setVisible(false);

		textField_7 = new JTextField();
		textField_7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_7.setColumns(10);
		textField_7.setBounds(36, 13, 221, 45);
		panel_2.add(textField_7);

		btnNewButton_1 = new JButton("\u00DCr\u00FCn Sil");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				urunDB.urunCikarma(txtOmer.getText());

			}
		});
		btnNewButton_1.setBounds(321, 14, 221, 45);
		panel_2.add(btnNewButton_1);

		
		panel_3.setLayout(null);
		panel_3.setBounds(25, 489, 1232, 310);
		panel_3.setVisible(false);
		
		panel.add(panel_3);

		gField1 = new JTextField();
		gField1.setText("Baknot ID");
		gField1.setForeground(Color.LIGHT_GRAY);
		gField1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField1.setColumns(10);
		gField1.setBounds(36, 111, 221, 45);
		panel_3.add(gField1);

		gField3 = new JTextField();
		gField3.setText("T\u00FCr");
		gField3.setForeground(Color.LIGHT_GRAY);
		gField3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField3.setColumns(10);
		gField3.setBounds(655, 111, 221, 45);
		panel_3.add(gField3);

		gField4 = new JTextField();
		gField4.setText("Marka");
		gField4.setForeground(Color.LIGHT_GRAY);
		gField4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField4.setColumns(10);
		gField4.setBounds(956, 111, 221, 45);
		panel_3.add(gField4);

		gField5 = new JTextField();
		gField5.setToolTipText("(birim fiyat\u0131)");
		gField5.setText("Al\u0131\u015F Fiyat\u0131");
		gField5.setForeground(Color.LIGHT_GRAY);
		gField5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField5.setColumns(10);
		gField5.setBounds(36, 183, 221, 45);
		panel_3.add(gField5);

		gField6 = new JTextField();
		gField6.setToolTipText("(birim fiyat\u0131)");
		gField6.setText("Sat\u0131\u015F Fiyat\u0131");
		gField6.setForeground(Color.LIGHT_GRAY);
		gField6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField6.setColumns(10);
		gField6.setBounds(350, 183, 221, 45);
		panel_3.add(gField6);

		gField7 = new JTextField();
		gField7.setText("Son Kullanma Tarihi");
		gField7.setForeground(Color.LIGHT_GRAY);
		gField7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField7.setColumns(10);
		gField7.setBounds(655, 183, 221, 45);
		panel_3.add(gField7);

		gField8 = new JTextField();
		gField8.setToolTipText("Doldurulmas\u0131 Zorunlu De\u011Fildir");
		gField8.setText("Stok Miktar\u0131");
		gField8.setForeground(Color.LIGHT_GRAY);
		gField8.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField8.setColumns(10);
		gField8.setBounds(956, 183, 221, 45);
		panel_3.add(gField8);

		button = new JButton("\u00DCr\u00FCn Ekle");
		button.setBounds(956, 252, 221, 45);
		panel_3.add(button);

		gField2 = new JTextField();
		gField2.setToolTipText("(birim fiyat\u0131)");
		gField2.setText("\u00DCr\u00FCn Ad\u0131");
		gField2.setForeground(Color.LIGHT_GRAY);
		gField2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gField2.setColumns(10);
		gField2.setBounds(350, 111, 221, 45);
		panel_3.add(gField2);

		textField_1 = new JTextField();
		textField_1.setText("Baknot ID");
		textField_1.setForeground(Color.LIGHT_GRAY);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_1.setColumns(10);
		textField_1.setBounds(350, 32, 221, 45);
		panel_3.add(textField_1);

		btnrnAra = new JButton("\u00DCr\u00FCn Ara");
		btnrnAra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				List<Object> a1 = new ArrayList<>();

				a1 = urunDB.tabloVerileri(textField_1.getText());

				gField1.setText((String) a1.get(0));
				gField2.setText((String) a1.get(1));
				gField3.setText((String) a1.get(2));
				gField4.setText((String) a1.get(3));
				gField5.setText((String) a1.get(4).toString());
				gField6.setText((String) a1.get(5).toString());
				gField7.setText((String) a1.get(6));
				gField8.setText((String) a1.get(7).toString());

			}
		});
		btnrnAra.setBounds(655, 33, 221, 45);
		panel_3.add(btnrnAra);
	}
}
