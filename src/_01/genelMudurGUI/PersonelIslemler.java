package _01.genelMudurGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import _01.DAO.PersonelIslemleriDB;

import javax.swing.JTabbedPane;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PersonelIslemler extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtTc;
	private JTextField txtAdsoyad;
	private JTextField txtKdem;
	private JTextField txtTelefon;
	private JTextField txtEmail;
	private JTextField txtAdres;
	private JTextField textField;

	JRadioButton rdbListe = new JRadioButton(
			"\u00DCst D\u00FCzey Y\u00F6netici Bilgilerini Listelemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbGuncelle = new JRadioButton(
			"\u00DCst D\u00FCzey Y\u00F6netici Bilgilerini G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbEkleme = new JRadioButton(
			"\u00DCst D\u00FCzey Y\u00F6netici Eklemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbYCikar = new JRadioButton(
			"Y\u00F6netici \u00C7\u0131karmak \u0130\u00E7in T\u0131klay\u0131n\u0131z..");

	JRadioButton rdbCikar = new JRadioButton(
			"\u00DCst D\u00FCzey Y\u00F6netici \u00C7\u0131karmak \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbYEkle = new JRadioButton("Y\u00F6netici Eklemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbYGuncelle = new JRadioButton(
			"Y\u00F6netici Bilgilerini G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbYListele = new JRadioButton(
			"Y\u00F6netici Bilgilerini Listelemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");

	JRadioButton rdbCEkle = new JRadioButton("Y\u00F6netici Eklemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbCGuncelle = new JRadioButton(
			"Y\u00F6netici Bilgilerini G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbCListele = new JRadioButton(
			"Y\u00F6netici Bilgilerini Listelemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
	JRadioButton rdbCCikar = new JRadioButton(
			"Y\u00F6netici \u00C7\u0131karmak \u0130\u00E7in T\u0131klay\u0131n\u0131z..");

	private JPanel panel_4;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private JTextField textField_5;
	private JButton button_2;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JButton button_3;
	private JPasswordField passwordField;
	private JPasswordField pwdParola;
	PersonelIslemleriDB p1 = new PersonelIslemleriDB();
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_20;
	private JTextField textField_21;
	private JTextField textField_22;
	private JTextField textField_23;
	private JTextField textField_24;
	private JTextField textField_3;
	private JTextField textField_9;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JPasswordField passwordField_1;
	private JPasswordField passwordField_2;
	private JTextField txtubeId;
	private JTextField textField_10;
	private JTextField txtC;
	private JTextField txtC2;
	private JTextField txtC4;
	private JTextField txtC5;
	private JTextField txtC6;
	private JTextField txtC7;
	private JPasswordField txtC3;
	private JTextField txtC8;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private JPanel panel_10;
	private JTextField txtCG;
	private JTextField txtCG2;
	private JTextField txtCG4;
	private JTextField textField_27;
	private JButton button_9;
	private JTextField txtCG5;
	private JTextField txtCG6;
	private JTextField txtCG7;
	private JButton button_10;
	private JPasswordField txtCG3;
	private JTextField txtCG8;
	private JPanel panel_11;
	private JTextField textField_31;
	private JButton button_11;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PersonelIslemler frame = new PersonelIslemler();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PersonelIslemler() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1300, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 17));
		tabbedPane.setBounds(12, 13, 1258, 827);
		contentPane.add(tabbedPane);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Genel Mudur ��lemleri", null, panel_1, null);
		panel_1.setLayout(null);

		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(12, 347, 1160, 205);
		panel_1.add(panel_2);
		panel_2.setVisible(false);

		panel_4 = new JPanel();
		panel_4.setBounds(0, 0, 1228, 293);
		panel_1.add(panel_4);
		panel_4.setLayout(null);
		panel_4.setVisible(false);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setToolTipText("T.C Kimlik Numaran\u0131z\u0131 Giriniz..");
		textField_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_1.getText().equals("T.C Numaras\u0131")) {

					textField_1.setForeground(Color.BLACK);
					textField_1.setText("");
				}
			}
		});
		textField_1.setText("T.C Numaras\u0131");
		textField_1.setForeground(Color.LIGHT_GRAY);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_1.setColumns(10);
		textField_1.setBounds(47, 107, 221, 45);
		panel_4.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setToolTipText("Ad-Soyad Giriniz..");
		textField_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_2.getText().equals("Ad-Soyad")) {

					textField_2.setForeground(Color.BLACK);
					textField_2.setText("");
				}
			}
		});
		textField_2.setText("Ad-Soyad");
		textField_2.setForeground(Color.LIGHT_GRAY);
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_2.setColumns(10);
		textField_2.setBounds(349, 107, 221, 45);
		panel_4.add(textField_2);

		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setToolTipText("1-5 Aras\u0131 K\u0131dem Seviyesini Giriniz.. ");
		textField_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_4.getText().equals("K\u0131dem")) {

					textField_4.setForeground(Color.BLACK);
					textField_4.setText("");
				}
			}
		});
		textField_4.setText("K\u0131dem");
		textField_4.setForeground(Color.LIGHT_GRAY);
		textField_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_4.setColumns(10);
		textField_4.setBounds(938, 107, 221, 45);
		panel_4.add(textField_4);

		textField_5 = new JTextField();
		textField_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (textField_5.getText().equals("T.C Numaras\u0131")) {

					textField_5.setForeground(Color.BLACK);
					textField_5.setText("");
				}

			}
		});
		textField_5.setText("T.C Numaras\u0131");
		textField_5.setForeground(Color.LIGHT_GRAY);
		textField_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_5.setColumns(10);
		textField_5.setBounds(349, 13, 221, 45);
		panel_4.add(textField_5);

		button_2 = new JButton("Ara");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				List<Object> a1 = p1.mudurVerileri(textField_5.getText());

				if (a1.get(0).toString().equals(textField_5.getText())) {

					textField_1.setEditable(true);
					textField_2.setEditable(true);
					textField_4.setEditable(true);
					textField_6.setEditable(true);
					textField_7.setEditable(true);
					textField_8.setEditable(true);
					pwdParola.setEditable(true);
					button_3.setEnabled(true);

					textField_1.setText(a1.get(0).toString());
					textField_2.setText(a1.get(1).toString());
					pwdParola.setText(a1.get(2).toString());
					textField_4.setText(a1.get(3).toString());
					textField_6.setText(a1.get(4).toString());

					textField_7.setText(a1.get(5).toString());
					textField_8.setText(a1.get(6).toString());

					textField_1.setForeground(Color.BLACK);
					textField_2.setForeground(Color.BLACK);
					textField_4.setForeground(Color.BLACK);
					textField_6.setForeground(Color.BLACK);
					textField_7.setForeground(Color.BLACK);
					textField_8.setForeground(Color.BLACK);

				} else {

					JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!",
							2);

				}

			}
		});
		button_2.setToolTipText(
				"B\u0130lgilerini G\u00FCncellemek \u0130stedi\u011Finiz Genel M\u00FCd\u00FCr\u00FCn T.C Numaras\u0131n\u0131 Giriniz...");
		button_2.setForeground(Color.BLACK);
		button_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_2.setBounds(643, 13, 221, 45);
		panel_4.add(button_2);

		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setToolTipText("Adres Giriniz..");
		textField_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_6.getText().equals("Adres")) {

					textField_6.setForeground(Color.BLACK);
					textField_6.setText("");
				}
			}
		});
		textField_6.setText("Adres");
		textField_6.setForeground(Color.LIGHT_GRAY);
		textField_6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_6.setColumns(10);
		textField_6.setBounds(47, 196, 221, 45);
		panel_4.add(textField_6);

		textField_7 = new JTextField();
		textField_7.setEditable(false);
		textField_7.setToolTipText("Telefon Giriniz..");
		textField_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_7.getText().equals("Telefon")) {

					textField_7.setForeground(Color.BLACK);
					textField_7.setText("");
				}
			}
		});
		textField_7.setText("Telefon");
		textField_7.setForeground(Color.LIGHT_GRAY);
		textField_7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_7.setColumns(10);
		textField_7.setBounds(349, 196, 221, 45);
		panel_4.add(textField_7);

		textField_8 = new JTextField();
		textField_8.setEditable(false);
		textField_8.setToolTipText("E-Mail Giriniz..");
		textField_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_8.getText().equals("E-Mail")) {

					textField_8.setForeground(Color.BLACK);
					textField_8.setText("");
				}
			}
		});
		textField_8.setText("E-Mail");
		textField_8.setForeground(Color.LIGHT_GRAY);
		textField_8.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_8.setColumns(10);
		textField_8.setBounds(643, 196, 221, 45);
		panel_4.add(textField_8);

		button_3 = new JButton("Guncelle");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p1.mudurGuncelle(textField_1.getText(), textField_2.getText(), pwdParola.getText(),
						textField_4.getText(), textField_6.getText(), textField_7.getText(), textField_8.getText());

				textField_1.setText("");
				textField_2.setText("");
				pwdParola.setText("");
				textField_4.setText("");
				textField_5.setText("");
				textField_6.setText("");
				textField_7.setText("");
				textField_8.setText("");

				textField_1.setEditable(false);
				textField_2.setEditable(false);
				textField_4.setEditable(false);
				textField_6.setEditable(false);
				textField_7.setEditable(false);
				textField_8.setEditable(false);
				pwdParola.setEditable(false);
				button_3.setEnabled(false);
			}
		});
		button_3.setEnabled(false);
		button_3.setToolTipText("Bilgileri G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		button_3.setForeground(Color.BLACK);
		button_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_3.setBounds(938, 196, 221, 45);
		panel_4.add(button_3);

		pwdParola = new JPasswordField();
		pwdParola.setEditable(false);
		pwdParola.setToolTipText("Parola Giriniz..");
		pwdParola.setBounds(643, 108, 221, 45);
		panel_4.add(pwdParola);

		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBounds(12, 608, 1160, 82);
		panel_1.add(panel_3);
		panel_3.setVisible(false);

		rdbCikar.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				rdbGuncelle.setBounds(8, 103, 566, 33);
				rdbListe.setBounds(8, 350, 566, 33);
				rdbCikar.setBounds(8, 191, 566, 33);

				panel_3.setBounds(12, 250, 1160, 82);

				panel_3.setVisible(true);
				panel_2.setVisible(false);
				panel_4.setVisible(false);
			}
		});

		rdbEkleme.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_2.setBounds(12, 81, 1160, 205);

				rdbGuncelle.setBounds(8, 280, 566, 33);
				rdbCikar.setBounds(8, 360, 566, 33);
				rdbListe.setBounds(8, 440, 566, 33);

				panel_2.setVisible(true);
				panel_3.setVisible(false);
				panel_4.setVisible(false);
			}
		});
		buttonGroup.add(rdbEkleme);
		rdbEkleme.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbEkleme.setBounds(8, 21, 566, 33);
		panel_1.add(rdbEkleme);

		buttonGroup.add(rdbGuncelle);
		rdbGuncelle.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_4.setBounds(12, 150, 1228, 293);

				rdbGuncelle.setBounds(8, 103, 566, 33);
				rdbCikar.setBounds(8, 440, 566, 33);
				rdbListe.setBounds(8, 520, 566, 33);

				panel_4.setVisible(true);
				panel_2.setVisible(false);
				panel_3.setVisible(false);

			}
		});
		rdbGuncelle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbGuncelle.setBounds(8, 103, 566, 33);
		panel_1.add(rdbGuncelle);

		buttonGroup.add(rdbCikar);
		rdbCikar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbCikar.setBounds(8, 191, 566, 33);
		panel_1.add(rdbCikar);

		buttonGroup.add(rdbListe);
		rdbListe.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbListe.setBounds(8, 288, 566, 33);
		panel_1.add(rdbListe);

		txtTc = new JTextField();
		txtTc.setToolTipText("T.C Giriniz..");
		txtTc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtTc.getText().equals("T.C")) {

					txtTc.setForeground(Color.BLACK);
					txtTc.setText("");
				}
			}
		});
		txtTc.setText("T.C");
		txtTc.setForeground(Color.LIGHT_GRAY);
		txtTc.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTc.setColumns(10);
		txtTc.setBounds(32, 13, 221, 45);
		panel_2.add(txtTc);

		JButton button = new JButton("Ekle");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p1.genelMudurEkleme(txtTc.getText(), txtAdsoyad.getText(), passwordField.getText(), txtKdem.getText(),
						txtAdres.getText(), txtTelefon.getText(), txtEmail.getText());

				txtTc.setText("");
				txtAdsoyad.setText("");
				passwordField.setText("");
				txtKdem.setText("");
				txtAdres.setText("");
				txtTelefon.setText("");
				txtEmail.setText("");
			}
		});
		button.setToolTipText("Eklemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		button.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button.setBounds(897, 109, 221, 45);
		panel_2.add(button);

		txtAdsoyad = new JTextField();
		txtAdsoyad.setToolTipText("Ad-Soyad Giriniz..");
		txtAdsoyad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtAdsoyad.getText().equals("Ad-Soyad")) {

					txtAdsoyad.setForeground(Color.BLACK);
					txtAdsoyad.setText("");
				}
			}
		});
		txtAdsoyad.setText("Ad-Soyad");
		txtAdsoyad.setForeground(Color.LIGHT_GRAY);
		txtAdsoyad.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdsoyad.setColumns(10);
		txtAdsoyad.setBounds(327, 13, 221, 45);
		panel_2.add(txtAdsoyad);

		txtKdem = new JTextField();
		txtKdem.setToolTipText("1-5 Aras\u0131 K\u0131dem Giriniz..");
		txtKdem.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtKdem.getText().equals("K\u0131dem")) {

					txtKdem.setForeground(Color.BLACK);
					txtKdem.setText("");
				}

			}
		});
		txtKdem.setText("K\u0131dem");
		txtKdem.setForeground(Color.LIGHT_GRAY);
		txtKdem.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtKdem.setColumns(10);
		txtKdem.setBounds(897, 13, 221, 45);
		panel_2.add(txtKdem);

		txtTelefon = new JTextField();
		txtTelefon.setToolTipText("Telefon Giriniz..");
		txtTelefon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtTelefon.getText().equals("Telefon")) {

					txtTelefon.setForeground(Color.BLACK);
					txtTelefon.setText("");
				}
			}
		});
		txtTelefon.setText("Telefon");
		txtTelefon.setForeground(Color.LIGHT_GRAY);
		txtTelefon.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTelefon.setColumns(10);
		txtTelefon.setBounds(327, 110, 221, 45);
		panel_2.add(txtTelefon);

		txtEmail = new JTextField();
		txtEmail.setToolTipText("E-Mail Giriniz..");
		txtEmail.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtEmail.getText().equals("E-Mail")) {

					txtEmail.setForeground(Color.BLACK);
					txtEmail.setText("");
				}
			}
		});
		txtEmail.setText("E-Mail");
		txtEmail.setForeground(Color.LIGHT_GRAY);
		txtEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtEmail.setColumns(10);
		txtEmail.setBounds(613, 110, 221, 45);
		panel_2.add(txtEmail);

		txtAdres = new JTextField();
		txtAdres.setToolTipText("Adres Giriniz..");
		txtAdres.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtAdres.getText().equals("Adres")) {

					txtAdres.setForeground(Color.BLACK);
					txtAdres.setText("");
				}

			}
		});
		txtAdres.setText("Adres");
		txtAdres.setForeground(Color.LIGHT_GRAY);
		txtAdres.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdres.setColumns(10);
		txtAdres.setBounds(32, 109, 221, 45);

		panel_2.add(txtAdres);

		passwordField = new JPasswordField();
		passwordField.setToolTipText("Parola Giriniz..");
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		passwordField.setBounds(613, 14, 221, 45);

		panel_2.add(passwordField);

		textField = new JTextField();
		textField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField.getText().equals("T.C Numaras\u0131")) {

					textField.setForeground(Color.BLACK);
					textField.setText("");
				}
			}
		});

		textField.setText("T.C Numaras\u0131");
		textField.setForeground(Color.LIGHT_GRAY);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setColumns(10);
		textField.setBounds(37, 13, 221, 47);
		panel_3.add(textField);

		JButton button_1 = new JButton("Kald\u0131r");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p1.genelMudurCikarma(textField.getText());

				textField.setText("");
			}
		});
		button_1.setForeground(Color.BLACK);
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_1.setBounds(270, 13, 221, 47);
		panel_3.add(button_1);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Y�netici ��lemleri", null, panel, null);
		panel.setLayout(null);

		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBounds(12, 270, 1229, 280);
		panel_5.setVisible(false);
		panel.add(panel_5);

		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBounds(12, 175, 1228, 82);
		panel_6.setVisible(false);
		panel.add(panel_6);

		JPanel panel_7 = new JPanel();
		panel_7.setLayout(null);
		panel_7.setBounds(56, 425, 1228, 337);
		panel_7.setVisible(false);
		panel.add(panel_7);

		buttonGroup_1.add(rdbYEkle);

		rdbYEkle.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_5.setBounds(12, 61, 1250, 282);

				rdbYGuncelle.setBounds(8, 340, 566, 33);
				rdbYCikar.setBounds(8, 420, 566, 33);
				rdbYListele.setBounds(8, 500, 566, 33);

				panel_5.setVisible(true);
				panel_6.setVisible(false);
				panel_7.setVisible(false);
			}
		});

		rdbYEkle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbYEkle.setBounds(12, 28, 566, 33);
		panel.add(rdbYEkle);

		buttonGroup_1.add(rdbYGuncelle);
		rdbYGuncelle.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_7.setBounds(12, 150, 1228, 337);

				rdbYGuncelle.setBounds(8, 103, 566, 33);
				rdbYCikar.setBounds(8, 500, 566, 33);
				rdbYListele.setBounds(8, 580, 566, 33);

				panel_7.setVisible(true);
				panel_6.setVisible(false);
				panel_5.setVisible(false);

			}
		});

		rdbYGuncelle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbYGuncelle.setBounds(12, 87, 566, 33);
		panel.add(rdbYGuncelle);
		buttonGroup_1.add(rdbYCikar);
		rdbYCikar.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				rdbYGuncelle.setBounds(8, 103, 566, 33);
				rdbYCikar.setBounds(8, 191, 566, 33);
				rdbYListele.setBounds(8, 350, 566, 33);

				panel_6.setBounds(12, 250, 1160, 82);

				panel_6.setVisible(true);
				panel_7.setVisible(false);
				panel_5.setVisible(false);
			}
		});

		rdbYCikar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbYCikar.setBounds(12, 152, 566, 33);
		panel.add(rdbYCikar);
		buttonGroup_1.add(rdbYListele);

		rdbYListele.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbYListele.setBounds(12, 216, 566, 33);
		panel.add(rdbYListele);

		textField_16 = new JTextField();
		textField_16.setToolTipText("T.C Numaran\u0131z\u0131 Giriniz..");
		textField_16.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_16.getText().equals("T.C Numaras\u0131")) {

					textField_16.setForeground(Color.BLACK);
					textField_16.setText("");
				}
			}
		});
		textField_16.setText("T.C Numaras\u0131");
		textField_16.setForeground(Color.LIGHT_GRAY);
		textField_16.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_16.setColumns(10);
		textField_16.setBounds(37, 13, 221, 45);
		panel_6.add(textField_16);

		JButton button_6 = new JButton("Kald\u0131r");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p1.yoneticiCikarma(textField_16.getText());

				textField_16.setText("");
			}
		});
		button_6.setToolTipText("Y\u00F6netici Kald\u0131rmak \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		button_6.setForeground(Color.BLACK);
		button_6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_6.setBounds(270, 13, 221, 45);
		panel_6.add(button_6);

		textField_17 = new JTextField();
		textField_17.setEditable(false);
		textField_17.setToolTipText("T.C Numaras\u0131 G\u0130riniz..");
		textField_17.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_17.getText().equals("T.C Numaras\u0131")) {

					textField_17.setForeground(Color.BLACK);
					textField_17.setText("");
				}
			}
		});
		textField_17.setText("T.C Numaras\u0131");
		textField_17.setForeground(Color.LIGHT_GRAY);
		textField_17.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_17.setColumns(10);
		textField_17.setBounds(47, 107, 221, 45);
		panel_7.add(textField_17);

		textField_18 = new JTextField();
		textField_18.setEditable(false);
		textField_18.setToolTipText("Ad-Soyad Giriniz..");
		textField_18.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_18.getText().equals("Ad-Soyad")) {

					textField_18.setForeground(Color.BLACK);
					textField_18.setText("");
				}
			}
		});
		textField_18.setText("Ad-Soyad");
		textField_18.setForeground(Color.LIGHT_GRAY);
		textField_18.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_18.setColumns(10);
		textField_18.setBounds(349, 107, 221, 45);
		panel_7.add(textField_18);

		textField_20 = new JTextField();
		textField_20.setEditable(false);
		textField_20.setToolTipText("1-5 Aras\u0131nda K\u0131dem Seviyesi Giriniz...");
		textField_20.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (textField_20.getText().equals("K\u0131dem")) {

					textField_20.setForeground(Color.BLACK);
					textField_20.setText("");
				}
			}
		});
		textField_20.setText("K\u0131dem");
		textField_20.setForeground(Color.LIGHT_GRAY);
		textField_20.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_20.setColumns(10);
		textField_20.setBounds(938, 107, 221, 45);
		panel_7.add(textField_20);

		textField_21 = new JTextField();
		textField_21.setToolTipText("T.C Numaras\u0131 Giriniz..");
		textField_21.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_21.getText().equals("T.C Numaras\u0131")) {

					textField_21.setForeground(Color.BLACK);
					textField_21.setText("");
				}
			}
		});
		textField_21.setText("T.C Numaras\u0131");
		textField_21.setForeground(Color.LIGHT_GRAY);
		textField_21.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_21.setColumns(10);
		textField_21.setBounds(349, 13, 221, 45);
		panel_7.add(textField_21);

		JButton button_8 = new JButton("Guncelle");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p1.yoneticiGuncelle(textField_17.getText(), textField_18.getText(), passwordField_2.getText(),
						textField_20.getText(), textField_22.getText(), textField_23.getText(), textField_24.getText(),
						textField_10.getText());

				textField_17.setText("");
				textField_18.setText("");
				textField_20.setText("");
				textField_21.setText("");
				textField_22.setText("");
				passwordField_2.setText("");
				textField_23.setText("");
				textField_24.setText("");
				textField_10.setText("");

				textField_17.setEditable(false);
				textField_18.setEditable(false);
				textField_20.setEditable(false);
				textField_22.setEditable(false);
				textField_23.setEditable(false);
				textField_24.setEditable(false);
				passwordField_2.setEditable(false);
				textField_10.setEditable(false);
				button_8.setEnabled(false);

			}
		});
		button_8.setEnabled(false);
		button_8.setToolTipText(
				"Y\u00F6netici Bilgilerini G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		button_8.setForeground(Color.BLACK);
		button_8.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_8.setBounds(959, 272, 221, 45);
		panel_7.add(button_8);

		JButton button_7 = new JButton("Ara");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				List<Object> a1 = p1.yoneticiVerileri(textField_21.getText());

				if (a1.get(0).toString().equals(textField_21.getText())) {

					textField_17.setEditable(true);
					textField_18.setEditable(true);
					textField_20.setEditable(true);
					textField_22.setEditable(true);
					textField_23.setEditable(true);
					textField_24.setEditable(true);
					passwordField_2.setEditable(true);
					textField_10.setEditable(true);
					button_8.setEnabled(true);

					textField_17.setText(a1.get(0).toString());
					textField_18.setText(a1.get(1).toString());
					passwordField_2.setText(a1.get(2).toString());
					textField_20.setText(a1.get(3).toString());
					textField_22.setText(a1.get(4).toString());
					textField_23.setText(a1.get(5).toString());
					textField_24.setText(a1.get(6).toString());
					textField_10.setText(a1.get(7).toString());

					textField_17.setForeground(Color.BLACK);
					textField_18.setForeground(Color.BLACK);
					textField_20.setForeground(Color.BLACK);
					textField_22.setForeground(Color.BLACK);
					textField_23.setForeground(Color.BLACK);
					textField_24.setForeground(Color.BLACK);
					textField_10.setForeground(Color.BLACK);

				} else {

					JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!",
							2);

				}

			}
		});
		button_7.setToolTipText(
				"Bilgilerini G\u00FCncellemek \u0130stedi\u011Finiz Y\u00F6neticiye Ait T.C Numaras\u0131n\u0131 Giriniz..");
		button_7.setForeground(Color.BLACK);
		button_7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_7.setBounds(643, 13, 221, 45);
		panel_7.add(button_7);

		textField_22 = new JTextField();
		textField_22.setEditable(false);
		textField_22.setToolTipText("Adres Bilgilerini Giriniz..");
		textField_22.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_22.getText().equals("Adres")) {

					textField_22.setForeground(Color.BLACK);
					textField_22.setText("");
				}
			}
		});
		textField_22.setText("Adres");
		textField_22.setForeground(Color.LIGHT_GRAY);
		textField_22.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_22.setColumns(10);
		textField_22.setBounds(47, 196, 221, 45);
		panel_7.add(textField_22);

		textField_23 = new JTextField();
		textField_23.setEditable(false);
		textField_23.setToolTipText("Telefon Numaras\u0131 Giriniz..");
		textField_23.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_23.getText().equals("Telefon")) {

					textField_23.setForeground(Color.BLACK);
					textField_23.setText("");
				}

			}
		});
		textField_23.setText("Telefon");
		textField_23.setForeground(Color.LIGHT_GRAY);
		textField_23.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_23.setColumns(10);
		textField_23.setBounds(349, 196, 221, 45);
		panel_7.add(textField_23);

		textField_24 = new JTextField();
		textField_24.setEditable(false);
		textField_24.setToolTipText("E-Mail Adresi Giriniz..");
		textField_24.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_24.getText().equals("E-Mail")) {

					textField_24.setForeground(Color.BLACK);
					textField_24.setText("");
				}
			}
		});
		textField_24.setText("E-Mail");
		textField_24.setForeground(Color.LIGHT_GRAY);
		textField_24.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_24.setColumns(10);
		textField_24.setBounds(643, 196, 221, 45);
		panel_7.add(textField_24);

		passwordField_2 = new JPasswordField();
		passwordField_2.setEditable(false);
		passwordField_2.setToolTipText("Parola Giriniz..");
		passwordField_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		passwordField_2.setBounds(643, 107, 221, 45);
		panel_7.add(passwordField_2);

		textField_10 = new JTextField();
		textField_10.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_10.getText().equals("\u015Eube ID")) {

					textField_10.setForeground(Color.BLACK);
					textField_10.setText("");
				}

			}
		});
		textField_10.setEditable(false);
		textField_10.setToolTipText("Y\u00F6neticinin Sorumlu Oldu\u011Fu \u015Eube'nin ID' sini Giriniz..");
		textField_10.setText("\u015Eube ID");
		textField_10.setForeground(Color.LIGHT_GRAY);
		textField_10.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_10.setColumns(10);
		textField_10.setBounds(938, 196, 221, 45);
		panel_7.add(textField_10);

		textField_3 = new JTextField();
		textField_3.setToolTipText("T.C Numaran\u0131z\u0131 Giriniz..");
		textField_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_3.getText().equals("T.C Numaras\u0131")) {

					textField_3.setForeground(Color.BLACK);
					textField_3.setText("");
				}

			}
		});
		textField_3.setText("T.C Numaras\u0131");
		textField_3.setForeground(Color.LIGHT_GRAY);
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_3.setColumns(10);
		textField_3.setBounds(33, 36, 221, 45);
		panel_5.add(textField_3);

		textField_9 = new JTextField();
		textField_9.setToolTipText("Ad-Soyad Giriniz..");
		textField_9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_9.getText().equals("Ad-Soyad")) {

					textField_9.setForeground(Color.BLACK);
					textField_9.setText("");
				}
			}
		});
		textField_9.setText("Ad-Soyad");
		textField_9.setForeground(Color.LIGHT_GRAY);
		textField_9.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_9.setColumns(10);
		textField_9.setBounds(328, 36, 221, 45);
		panel_5.add(textField_9);

		textField_11 = new JTextField();
		textField_11.setToolTipText("1-5 Aras\u0131nda K\u0131dem Seviyesi Giriniz...");
		textField_11.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (textField_11.getText().equals("K\u0131dem")) {

					textField_11.setForeground(Color.BLACK);
					textField_11.setText("");
				}
			}
		});
		textField_11.setText("K\u0131dem");
		textField_11.setForeground(Color.LIGHT_GRAY);
		textField_11.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_11.setColumns(10);
		textField_11.setBounds(929, 36, 221, 45);
		panel_5.add(textField_11);

		textField_12 = new JTextField();
		textField_12.setToolTipText("Adres B\u0130lgilerini Giriniz...");
		textField_12.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (textField_12.getText().equals("Adres")) {

					textField_12.setForeground(Color.BLACK);
					textField_12.setText("");
				}

			}
		});
		textField_12.setText("Adres");
		textField_12.setForeground(Color.LIGHT_GRAY);
		textField_12.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_12.setColumns(10);
		textField_12.setBounds(33, 130, 221, 45);
		panel_5.add(textField_12);

		textField_13 = new JTextField();
		textField_13.setToolTipText("Telefon Numaras\u0131 Giriniz..");
		textField_13.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_13.getText().equals("Telefon")) {

					textField_13.setForeground(Color.BLACK);
					textField_13.setText("");
				}
			}
		});
		textField_13.setText("Telefon");
		textField_13.setForeground(Color.LIGHT_GRAY);
		textField_13.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_13.setColumns(10);
		textField_13.setBounds(328, 130, 221, 45);
		panel_5.add(textField_13);

		textField_14 = new JTextField();
		textField_14.setToolTipText("E-Mail Giriniz...");
		textField_14.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (textField_14.getText().equals("E-Mail")) {

					textField_14.setForeground(Color.BLACK);
					textField_14.setText("");
				}
			}
		});
		textField_14.setText("E-Mail");
		textField_14.setForeground(Color.LIGHT_GRAY);
		textField_14.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_14.setColumns(10);
		textField_14.setBounds(627, 130, 221, 45);
		panel_5.add(textField_14);

		txtubeId = new JTextField();

		txtubeId.setToolTipText("Y\u00F6neticinin Sorumlu Oldu\u011Fu \u015Eube'nin ID' sini Giriniz..");
		txtubeId.setText("\u015Eube ID");
		txtubeId.setForeground(Color.LIGHT_GRAY);
		txtubeId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtubeId.setColumns(10);
		txtubeId.setBounds(929, 130, 221, 45);
		panel_5.add(txtubeId);
		txtubeId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtubeId.getText().equals("\u015Eube ID")) {

					txtubeId.setForeground(Color.BLACK);
					txtubeId.setText("");
				}
			}
		});

		JButton button_4 = new JButton("Ekle");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				p1.yoneticiEkleme(textField_3.getText(), textField_9.getText(), passwordField_1.getText(),
						textField_11.getText(), textField_12.getText(), textField_13.getText(), textField_14.getText(),
						txtubeId.getText());

				textField_3.setText("");
				textField_9.setText("");
				passwordField_1.setText("");
				textField_11.setText("");
				textField_12.setText("");
				textField_13.setText("");
				textField_14.setText("");
				txtubeId.setText("");

			}
		});
		button_4.setToolTipText("Y\u00F6netici Eklemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		button_4.setForeground(Color.BLACK);
		button_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_4.setBounds(929, 222, 221, 45);
		panel_5.add(button_4);

		passwordField_1 = new JPasswordField();
		passwordField_1.setToolTipText("Parola Giriniz..");
		passwordField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		passwordField_1.setBounds(627, 36, 221, 45);
		panel_5.add(passwordField_1);

		JPanel panel_8 = new JPanel();
		tabbedPane.addTab("Cali�an ��lemleri", null, panel_8, null);
		panel_8.setLayout(null);

		JPanel panel_9 = new JPanel();
		panel_9.setLayout(null);
		panel_9.setBounds(8, 88, 1228, 302);
		panel_9.setVisible(false);
		panel_8.add(panel_9);

		panel_10 = new JPanel();
		panel_10.setLayout(null);
		panel_10.setBounds(8, 291, 1228, 379);
		panel_10.setVisible(false);
		panel_8.add(panel_10);

		panel_11 = new JPanel();
		panel_11.setLayout(null);
		panel_11.setBounds(8, 697, 1228, 82);
		panel_11.setVisible(false);
		panel_8.add(panel_11);

		rdbCEkle.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_9.setBounds(12, 61, 1250, 302);

				rdbCGuncelle.setBounds(8, 400, 566, 33);
				rdbCCikar.setBounds(8, 480, 566, 33);
				rdbCListele.setBounds(8, 560, 566, 33);

				panel_9.setVisible(true);
				panel_10.setVisible(false);
				panel_11.setVisible(false);
			}
		});
		buttonGroup_2.add(rdbCEkle);
		rdbCEkle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbCEkle.setBounds(8, 21, 566, 33);
		panel_8.add(rdbCEkle);

		buttonGroup_2.add(rdbCGuncelle);
		rdbCGuncelle.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_10.setBounds(12, 150, 1228, 379);

				rdbCGuncelle.setBounds(8, 103, 566, 33);
				rdbCCikar.setBounds(8, 540, 566, 33);
				rdbCListele.setBounds(8, 620, 566, 33);

				panel_10.setVisible(true);
				panel_9.setVisible(false);
				panel_11.setVisible(false);

			}
		});
		rdbCGuncelle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbCGuncelle.setBounds(8, 88, 566, 33);
		panel_8.add(rdbCGuncelle);

		buttonGroup_2.add(rdbCCikar);
		rdbCCikar.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				rdbCGuncelle.setBounds(8, 103, 566, 33);
				rdbCCikar.setBounds(8, 191, 566, 33);
				rdbCListele.setBounds(8, 350, 566, 33);

				panel_11.setBounds(12, 250, 1160, 82);

				panel_11.setVisible(true);
				panel_9.setVisible(false);
				panel_10.setVisible(false);

			}
		});
		rdbCCikar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbCCikar.setBounds(8, 166, 566, 33);
		panel_8.add(rdbCCikar);

		buttonGroup_2.add(rdbCListele);
		rdbCListele.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbCListele.setBounds(8, 237, 566, 33);
		panel_8.add(rdbCListele);

		txtC = new JTextField();
		txtC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtC.getText().equals("T.C Numaras\u0131")) {

					txtC.setForeground(Color.BLACK);
					txtC.setText("");
				}

			}
		});
		txtC.setText("T.C Numaras\u0131");
		txtC.setForeground(Color.LIGHT_GRAY);
		txtC.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtC.setColumns(10);
		txtC.setBounds(47, 36, 221, 45);
		panel_9.add(txtC);

		txtC2 = new JTextField();
		txtC2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtC2.getText().equals("Ad-Soyad")) {

					txtC2.setForeground(Color.BLACK);
					txtC2.setText("");
				}

			}
		});
		txtC2.setText("Ad-Soyad");
		txtC2.setForeground(Color.LIGHT_GRAY);
		txtC2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtC2.setColumns(10);
		txtC2.setBounds(349, 36, 221, 45);
		panel_9.add(txtC2);

		txtC4 = new JTextField();
		txtC4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtC4.getText().equals("Bolum")) {

					txtC4.setForeground(Color.BLACK);
					txtC4.setText("");
				}

			}
		});
		txtC4.setText("Bolum");
		txtC4.setForeground(Color.LIGHT_GRAY);
		txtC4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtC4.setColumns(10);
		txtC4.setBounds(938, 36, 221, 45);
		panel_9.add(txtC4);

		txtC5 = new JTextField();
		txtC5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtC5.getText().equals("Adres")) {

					txtC5.setForeground(Color.BLACK);
					txtC5.setText("");
				}
			}
		});
		txtC5.setText("Adres");
		txtC5.setForeground(Color.LIGHT_GRAY);
		txtC5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtC5.setColumns(10);
		txtC5.setBounds(47, 130, 221, 45);
		panel_9.add(txtC5);

		txtC6 = new JTextField();
		txtC6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtC6.getText().equals("Telefon")) {

					txtC6.setForeground(Color.BLACK);
					txtC6.setText("");
				}
			}
		});
		txtC6.setText("Telefon");
		txtC6.setForeground(Color.LIGHT_GRAY);
		txtC6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtC6.setColumns(10);
		txtC6.setBounds(349, 130, 221, 45);
		panel_9.add(txtC6);

		txtC7 = new JTextField();
		txtC7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtC7.getText().equals("E-Mail")) {

					txtC7.setForeground(Color.BLACK);
					txtC7.setText("");
				}
			}
		});
		txtC7.setText("E-Mail");
		txtC7.setForeground(Color.LIGHT_GRAY);
		txtC7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtC7.setColumns(10);
		txtC7.setBounds(632, 130, 221, 45);
		panel_9.add(txtC7);

		JButton button_5 = new JButton("Ekle");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				p1.calisanEkleme(txtC.getText(), txtC2.getText(), txtC3.getText(),
						txtC4.getText(), txtC5.getText(), txtC6.getText(), txtC7.getText(),
						txtC8.getText());

				textField_3.setText("");
				textField_9.setText("");
				passwordField_1.setText("");
				textField_11.setText("");
				textField_12.setText("");
				textField_13.setText("");
				textField_14.setText("");
				txtubeId.setText("");
			}
		});
		button_5.setForeground(Color.BLACK);
		button_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_5.setBounds(938, 220, 221, 45);
		panel_9.add(button_5);

		txtC3 = new JPasswordField();
		txtC3.setBounds(632, 37, 221, 45);
		panel_9.add(txtC3);

		txtC8 = new JTextField();
		txtC8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtC8.getText().equals("\u015Eube ID")) {

					txtC8.setForeground(Color.BLACK);
					txtC8.setText("");
				}
			}
		});
		txtC8.setText("\u015Eube ID");
		txtC8.setForeground(Color.LIGHT_GRAY);
		txtC8.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtC8.setColumns(10);
		txtC8.setBounds(938, 130, 221, 45);
		panel_9.add(txtC8);

		txtCG = new JTextField();
		txtCG.setEditable(false);
		txtCG.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtCG.getText().equals("T.C Numaras\u0131")) {

					txtCG.setForeground(Color.BLACK);
					txtCG.setText("");
				}
			}
		});
		txtCG.setText("T.C Numaras\u0131");
		txtCG.setForeground(Color.LIGHT_GRAY);
		txtCG.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtCG.setColumns(10);
		txtCG.setBounds(47, 107, 221, 45);
		panel_10.add(txtCG);

		txtCG2 = new JTextField();
		txtCG2.setEditable(false);
		txtCG2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtCG2.getText().equals("Ad-Soyad")) {

					txtCG2.setForeground(Color.BLACK);
					txtCG2.setText("");
				}

			}
		});
		txtCG2.setText("Ad-Soyad");
		txtCG2.setForeground(Color.LIGHT_GRAY);
		txtCG2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtCG2.setColumns(10);
		txtCG2.setBounds(349, 107, 221, 45);
		panel_10.add(txtCG2);

		txtCG4 = new JTextField();
		txtCG4.setEditable(false);
		txtCG4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtCG4.getText().equals("Bolum")) {

					txtCG4.setForeground(Color.BLACK);
					txtCG4.setText("");
				}
			}
		});
		txtCG4.setText("Bolum");
		txtCG4.setForeground(Color.LIGHT_GRAY);
		txtCG4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtCG4.setColumns(10);
		txtCG4.setBounds(938, 107, 221, 45);
		panel_10.add(txtCG4);

		textField_27 = new JTextField();
		textField_27.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_27.getText().equals("T.C Numaras\u0131")) {

					textField_27.setForeground(Color.BLACK);
					textField_27.setText("");
				}
			}
		});
		textField_27.setText("T.C Numaras\u0131");
		textField_27.setForeground(Color.LIGHT_GRAY);
		textField_27.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_27.setColumns(10);
		textField_27.setBounds(349, 13, 221, 45);
		panel_10.add(textField_27);

		button_9 = new JButton("Ara");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				List<Object> a1 = p1.calisanVerileri(textField_27.getText());
				System.out.println(a1);
				if (a1.get(0).toString().equals(textField_27.getText())) {

					txtCG.setEditable(true);
					txtCG2.setEditable(true);
					txtCG3.setEditable(true);
					txtCG4.setEditable(true);
					txtCG5.setEditable(true);
					txtCG6.setEditable(true);
					txtCG7.setEditable(true);
					txtCG8.setEditable(true);
					button_10.setEnabled(true);

					txtCG.setText(a1.get(0).toString());
					txtCG2.setText(a1.get(1).toString());
					txtCG3.setText(a1.get(2).toString());
					txtCG4.setText(a1.get(3).toString());
					txtCG5.setText(a1.get(4).toString());
					txtCG6.setText(a1.get(5).toString());
					txtCG7.setText(a1.get(6).toString());
					txtCG8.setText(a1.get(7).toString());

					txtCG.setForeground(Color.BLACK);
					txtCG2.setForeground(Color.BLACK);
					txtCG4.setForeground(Color.BLACK);
					txtCG5.setForeground(Color.BLACK);
					txtCG6.setForeground(Color.BLACK);
					txtCG7.setForeground(Color.BLACK);
					txtCG8.setForeground(Color.BLACK);

				} else {

					JOptionPane.showMessageDialog(null, "Yanl�� T.C numaras� girdiniz. L�tfen Kontrol edin", "HATA!!!",
							2);

				}

				
				
				
			}
		});
		button_9.setForeground(Color.BLACK);
		button_9.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_9.setBounds(643, 13, 221, 45);
		panel_10.add(button_9);

		txtCG5 = new JTextField();
		txtCG5.setEditable(false);
		txtCG5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtCG5.getText().equals("Adres")) {

					txtCG5.setForeground(Color.BLACK);
					txtCG5.setText("");
				}
			}
		});
		txtCG5.setText("Adres");
		txtCG5.setForeground(Color.LIGHT_GRAY);
		txtCG5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtCG5.setColumns(10);
		txtCG5.setBounds(47, 196, 221, 45);
		panel_10.add(txtCG5);

		txtCG6 = new JTextField();
		txtCG6.setEditable(false);
		txtCG6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtCG6.getText().equals("Telefon")) {

					txtCG6.setForeground(Color.BLACK);
					txtCG6.setText("");
				}
			}
		});
		txtCG6.setText("Telefon");
		txtCG6.setForeground(Color.LIGHT_GRAY);
		txtCG6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtCG6.setColumns(10);
		txtCG6.setBounds(349, 196, 221, 45);
		panel_10.add(txtCG6);

		txtCG7 = new JTextField();
		txtCG7.setEditable(false);
		txtCG7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtCG7.getText().equals("E-Mail")) {

					txtCG7.setForeground(Color.BLACK);
					txtCG7.setText("");
				}
			}
		});
		txtCG7.setText("E-Mail");
		txtCG7.setForeground(Color.LIGHT_GRAY);
		txtCG7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtCG7.setColumns(10);
		txtCG7.setBounds(643, 196, 221, 45);
		panel_10.add(txtCG7);

		button_10 = new JButton("Guncelle");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				

				p1.calisanGuncelle(txtCG.getText(), txtCG2.getText(), txtCG3.getText(),
						txtCG4.getText(), txtCG5.getText(), txtCG6.getText(), txtCG7.getText(),txtCG8.getText());

				txtCG.setText("");
				txtCG2.setText("");
				txtCG3.setText("");
				txtCG4.setText("");
				txtCG5.setText("");
				txtCG6.setText("");
				txtCG7.setText("");
				txtCG8.setText("");

				txtCG.setEditable(false);
				txtCG2.setEditable(false);
				txtCG3.setEditable(false);
				txtCG4.setEditable(false);
				txtCG5.setEditable(false);
				txtCG6.setEditable(false);
				txtCG7.setEditable(false);
				txtCG8.setEditable(false);
				button_10.setEnabled(false);
			}
		});
		button_10.setEnabled(false);
		button_10.setForeground(Color.BLACK);
		button_10.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_10.setBounds(938, 291, 221, 45);
		panel_10.add(button_10);

		txtCG3 = new JPasswordField();
		txtCG3.setEditable(false);
		txtCG3.setBounds(643, 107, 221, 45);
		panel_10.add(txtCG3);

		txtCG8 = new JTextField();
		txtCG8.setEditable(false);
		txtCG8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (txtCG8.getText().equals("\u015Eube ID")) {

					txtCG8.setForeground(Color.BLACK);
					txtCG8.setText("");
				}
			}
		});
		txtCG8.setText("\u015Eube ID");
		txtCG8.setForeground(Color.LIGHT_GRAY);
		txtCG8.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtCG8.setColumns(10);
		txtCG8.setBounds(938, 196, 221, 45);
		panel_10.add(txtCG8);

		textField_31 = new JTextField();
		textField_31.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (textField_31.getText().equals("T.C Numaras\u0131")) {

					textField_31.setForeground(Color.BLACK);
					textField_31.setText("");
				}
			}
		});
		textField_31.setText("T.C Numaras\u0131");
		textField_31.setForeground(Color.LIGHT_GRAY);
		textField_31.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_31.setColumns(10);
		textField_31.setBounds(37, 13, 221, 45);
		panel_11.add(textField_31);

		button_11 = new JButton("Kald\u0131r");
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				p1.calisanCikarma(textField_31.getText());

				textField_31.setText("");
				
				
			}
		});
		button_11.setForeground(Color.BLACK);
		button_11.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_11.setBounds(270, 13, 221, 45);
		panel_11.add(button_11);

	}
}
