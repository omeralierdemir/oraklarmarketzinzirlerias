package _01.genelMudurGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JRadioButton;
import java.awt.Color;
import javax.swing.ButtonGroup;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class PersonelIslemleri extends JFrame {

	private JPanel contentPane;
	private JTextField txtTcNumaras;
	private JTextField txtAdsoyad;
	private JTextField txtParola;
	private JTextField txtKdem;
	private JTextField txtAdres;
	private JTextField txtTelefon;
	private JTextField txtEmail;
	private JRadioButton rdbGuncelle;
	private JPanel panel_3;
	private JTextField txtTcNumaras_2;
	private JTextField txtParola_1;
	private JTextField txtAdres_1;
	private JTextField txtTelefon_1;
	private JTextField txtAdsoyad_1;
	private JTextField txtEmail_1;
	private JButton button_1;
	private JTextField txtKdem_1;
	private JTextField txtTcNumaras_1;
	private JButton button_2;
	private JRadioButton rdbListele;
	private JPanel panel_4;
	private JTextField textField;
	private JButton button_3;
	private JRadioButton rdbCikar;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PersonelIslemleri frame = new PersonelIslemleri();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PersonelIslemleri() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1300, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 17));
		tabbedPane.setBounds(12, 52, 1258, 788);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("New tabbbbbbbbb", null, panel, null);
		panel.setLayout(null);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("New tab", null, panel_1, null);
		
		panel_4 = new JPanel();
		panel_4.setBounds(18, 59, 1160, 82);
		panel.add(panel_4);
		panel_4.setLayout(null);
		panel_4.setVisible(false);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(35, 430, 1160, 205);
		panel.add(panel_2);
		panel_2.setLayout(null);
		panel_2.setVisible(false);
		
		
		panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBounds(35, 439, 1160, 301);
		panel.add(panel_3);
		panel_3.setVisible(false);


		JRadioButton rdbEkle = new JRadioButton(
				"\u00DCst D\u00FCzey Y\u00F6netici Eklemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		rdbEkle.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				panel_2.setBounds(8, 76, 566, 33);

				rdbGuncelle.setBounds(8, 350, 566, 33);
				rdbCikar.setBounds(8, 400, 566, 33);
				rdbListele.setBounds(8, 450, 566, 33);

				panel_2.setVisible(true);

			}
		});

		buttonGroup.add(rdbEkle);
		rdbEkle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbEkle.setBounds(8, 26, 566, 33);
		panel.add(rdbEkle);

		rdbGuncelle = new JRadioButton(
				"\u00DCst D\u00FCzey Y\u00F6netici Bilgilerini G\u00FCncellemek \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		buttonGroup.add(rdbGuncelle);
		rdbGuncelle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbGuncelle.setBounds(8, 150, 566, 33);
		panel.add(rdbGuncelle);

		rdbCikar = new JRadioButton(
				"\u00DCst D\u00FCzey Y\u00F6netici \u00C7\u0131karmak \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		buttonGroup.add(rdbCikar);
		rdbCikar.setBounds(8, 210, 566, 33);
		panel.add(rdbCikar);
		rdbCikar.setFont(new Font("Tahoma", Font.PLAIN, 15));

		rdbListele = new JRadioButton(
				"\u00DCst D\u00FCzey Y\u00F6netici Listesi \u0130\u00E7in T\u0131klay\u0131n\u0131z..");
		rdbListele.setBounds(0, 0, 1253, 753);
		panel.add(rdbListele);
		buttonGroup.add(rdbListele);
		rdbListele.setFont(new Font("Tahoma", Font.PLAIN, 15));

		

		txtTcNumaras_2 = new JTextField();
		txtTcNumaras_2.setText("T.C Numaras\u0131");
		txtTcNumaras_2.setForeground(Color.LIGHT_GRAY);
		txtTcNumaras_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTcNumaras_2.setColumns(10);
		txtTcNumaras_2.setBounds(26, 103, 221, 47);
		panel_3.add(txtTcNumaras_2);

		txtParola_1 = new JTextField();
		txtParola_1.setText("Parola");
		txtParola_1.setForeground(Color.LIGHT_GRAY);
		txtParola_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtParola_1.setColumns(10);
		txtParola_1.setBounds(604, 103, 221, 47);
		panel_3.add(txtParola_1);

		txtAdres_1 = new JTextField();
		txtAdres_1.setToolTipText("");
		txtAdres_1.setText("Adres");
		txtAdres_1.setForeground(Color.LIGHT_GRAY);
		txtAdres_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdres_1.setColumns(10);
		txtAdres_1.setBounds(26, 196, 221, 47);
		panel_3.add(txtAdres_1);

		txtTelefon_1 = new JTextField();
		txtTelefon_1.setToolTipText("");
		txtTelefon_1.setText("Telefon");
		txtTelefon_1.setForeground(Color.LIGHT_GRAY);
		txtTelefon_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTelefon_1.setColumns(10);
		txtTelefon_1.setBounds(313, 196, 221, 47);
		panel_3.add(txtTelefon_1);

		txtAdsoyad_1 = new JTextField();
		txtAdsoyad_1.setText("Ad-Soyad");
		txtAdsoyad_1.setForeground(Color.LIGHT_GRAY);
		txtAdsoyad_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdsoyad_1.setColumns(10);
		txtAdsoyad_1.setBounds(313, 103, 221, 47);
		panel_3.add(txtAdsoyad_1);

		txtEmail_1 = new JTextField();
		txtEmail_1.setToolTipText("");
		txtEmail_1.setText("E-Mail");
		txtEmail_1.setForeground(Color.LIGHT_GRAY);
		txtEmail_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtEmail_1.setColumns(10);
		txtEmail_1.setBounds(604, 196, 221, 47);
		panel_3.add(txtEmail_1);

		button_1 = new JButton("\u00DCr\u00FCn Ekle");
		button_1.setBounds(885, 197, 227, 47);
		panel_3.add(button_1);

		txtKdem_1 = new JTextField();
		txtKdem_1.setToolTipText("(1-5 aras\u0131)");
		txtKdem_1.setText("K\u0131dem");
		txtKdem_1.setForeground(Color.LIGHT_GRAY);
		txtKdem_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtKdem_1.setColumns(10);
		txtKdem_1.setBounds(891, 103, 221, 47);
		panel_3.add(txtKdem_1);

		txtTcNumaras_1 = new JTextField();
		txtTcNumaras_1.setText("T.C Numaras\u0131");
		txtTcNumaras_1.setForeground(Color.LIGHT_GRAY);
		txtTcNumaras_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTcNumaras_1.setColumns(10);
		txtTcNumaras_1.setBounds(313, 32, 221, 47);
		panel_3.add(txtTcNumaras_1);

		button_2 = new JButton("\u00DCr\u00FCn Ara");
		button_2.setBounds(604, 33, 221, 47);
		panel_3.add(button_2);

		
		txtTcNumaras = new JTextField();
		txtTcNumaras.setText("T.C Numaras\u0131");
		txtTcNumaras.setForeground(Color.LIGHT_GRAY);
		txtTcNumaras.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTcNumaras.setColumns(10);
		txtTcNumaras.setBounds(32, 13, 221, 45);
		panel_2.add(txtTcNumaras);

		txtAdsoyad = new JTextField();
		txtAdsoyad.setText("Ad-Soyad");
		txtAdsoyad.setForeground(Color.LIGHT_GRAY);
		txtAdsoyad.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdsoyad.setColumns(10);
		txtAdsoyad.setBounds(328, 13, 221, 45);
		panel_2.add(txtAdsoyad);

		txtParola = new JTextField();
		txtParola.setText("Parola");
		txtParola.setForeground(Color.LIGHT_GRAY);
		txtParola.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtParola.setColumns(10);
		txtParola.setBounds(622, 13, 221, 45);
		panel_2.add(txtParola);

		txtKdem = new JTextField();
		txtKdem.setText("K\u0131dem");
		txtKdem.setForeground(Color.LIGHT_GRAY);
		txtKdem.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtKdem.setColumns(10);
		txtKdem.setBounds(897, 13, 221, 45);
		panel_2.add(txtKdem);

		txtAdres = new JTextField();
		txtAdres.setForeground(Color.LIGHT_GRAY);
		txtAdres.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdres.setText("Adres");
		txtAdres.setColumns(10);
		txtAdres.setBounds(32, 110, 221, 45);
		panel_2.add(txtAdres);

		txtTelefon = new JTextField();
		txtTelefon.setText("Telefon");
		txtTelefon.setForeground(Color.LIGHT_GRAY);
		txtTelefon.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTelefon.setColumns(10);
		txtTelefon.setBounds(328, 110, 221, 45);
		panel_2.add(txtTelefon);

		txtEmail = new JTextField();
		txtEmail.setText("E-Mail");
		txtEmail.setForeground(Color.LIGHT_GRAY);
		txtEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtEmail.setColumns(10);
		txtEmail.setBounds(622, 110, 221, 45);
		panel_2.add(txtEmail);

		JButton button = new JButton("Ekle");
		button.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button.setBounds(897, 109, 221, 45);
		panel_2.add(button);

		

		textField = new JTextField();
		textField.setText("T.C Numaras\u0131");
		textField.setForeground(Color.LIGHT_GRAY);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setColumns(10);
		textField.setBounds(37, 13, 221, 47);
		panel_4.add(textField);

		button_3 = new JButton("Kald\u0131r");
		button_3.setForeground(Color.BLACK);
		button_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_3.setBounds(270, 13, 221, 47);
		panel_4.add(button_3);

	}
}
