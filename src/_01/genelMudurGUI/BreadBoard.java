package _01.genelMudurGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;

public class BreadBoard extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField txtAdsoyad;
	private JTextField txtParola;
	private JTextField txtKdem;
	private JTextField textField_5;
	private JTextField txtAdres;
	private JTextField txtTelefon;
	private JTextField txtEmail;
	private JPanel panel_2;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JButton btnEkle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BreadBoard frame = new BreadBoard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BreadBoard() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1300, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(30, 33, 1228, 82);
		contentPane.add(panel);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setText("T.C Numaras\u0131");
		textField.setForeground(Color.LIGHT_GRAY);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setColumns(10);
		textField.setBounds(37, 13, 221, 45);
		panel.add(textField);
		
		JButton btnNewButton = new JButton("Kald\u0131r");
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.setBounds(270, 13, 221, 45);
		panel.add(btnNewButton);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(30, 178, 1228, 293);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setText("T.C Numaras\u0131");
		textField_1.setForeground(Color.LIGHT_GRAY);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_1.setColumns(10);
		textField_1.setBounds(47, 107, 221, 45);
		panel_1.add(textField_1);
		
		txtAdsoyad = new JTextField();
		txtAdsoyad.setText("Ad-Soyad");
		txtAdsoyad.setForeground(Color.LIGHT_GRAY);
		txtAdsoyad.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdsoyad.setColumns(10);
		txtAdsoyad.setBounds(349, 107, 221, 45);
		panel_1.add(txtAdsoyad);
		
		txtParola = new JTextField();
		txtParola.setText("Parola");
		txtParola.setForeground(Color.LIGHT_GRAY);
		txtParola.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtParola.setColumns(10);
		txtParola.setBounds(643, 107, 221, 45);
		panel_1.add(txtParola);
		
		txtKdem = new JTextField();
		txtKdem.setText("K\u0131dem");
		txtKdem.setForeground(Color.LIGHT_GRAY);
		txtKdem.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtKdem.setColumns(10);
		txtKdem.setBounds(938, 107, 221, 45);
		panel_1.add(txtKdem);
		
		textField_5 = new JTextField();
		textField_5.setText("T.C Numaras\u0131");
		textField_5.setForeground(Color.LIGHT_GRAY);
		textField_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_5.setColumns(10);
		textField_5.setBounds(349, 13, 221, 45);
		panel_1.add(textField_5);
		
		JButton btnAra = new JButton("Ara");
		btnAra.setForeground(Color.BLACK);
		btnAra.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAra.setBounds(643, 13, 221, 45);
		panel_1.add(btnAra);
		
		txtAdres = new JTextField();
		txtAdres.setText("Adres");
		txtAdres.setForeground(Color.LIGHT_GRAY);
		txtAdres.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAdres.setColumns(10);
		txtAdres.setBounds(47, 196, 221, 45);
		panel_1.add(txtAdres);
		
		txtTelefon = new JTextField();
		txtTelefon.setText("Telefon");
		txtTelefon.setForeground(Color.LIGHT_GRAY);
		txtTelefon.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtTelefon.setColumns(10);
		txtTelefon.setBounds(349, 196, 221, 45);
		panel_1.add(txtTelefon);
		
		txtEmail = new JTextField();
		txtEmail.setText("E-Mail");
		txtEmail.setForeground(Color.LIGHT_GRAY);
		txtEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtEmail.setColumns(10);
		txtEmail.setBounds(643, 196, 221, 45);
		panel_1.add(txtEmail);
		
		JButton btnGuncelle = new JButton("Guncelle");
		btnGuncelle.setForeground(Color.BLACK);
		btnGuncelle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnGuncelle.setBounds(938, 196, 221, 45);
		panel_1.add(btnGuncelle);
		
		panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(40, 474, 1228, 209);
		contentPane.add(panel_2);
		
		textField_2 = new JTextField();
		textField_2.setText("T.C Numaras\u0131");
		textField_2.setForeground(Color.LIGHT_GRAY);
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_2.setColumns(10);
		textField_2.setBounds(47, 36, 221, 45);
		panel_2.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setText("Ad-Soyad");
		textField_3.setForeground(Color.LIGHT_GRAY);
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_3.setColumns(10);
		textField_3.setBounds(349, 36, 221, 45);
		panel_2.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setText("Parola");
		textField_4.setForeground(Color.LIGHT_GRAY);
		textField_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_4.setColumns(10);
		textField_4.setBounds(632, 36, 221, 45);
		panel_2.add(textField_4);
		
		textField_6 = new JTextField();
		textField_6.setText("K\u0131dem");
		textField_6.setForeground(Color.LIGHT_GRAY);
		textField_6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_6.setColumns(10);
		textField_6.setBounds(938, 36, 221, 45);
		panel_2.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setText("Adres");
		textField_7.setForeground(Color.LIGHT_GRAY);
		textField_7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_7.setColumns(10);
		textField_7.setBounds(47, 130, 221, 45);
		panel_2.add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setText("Telefon");
		textField_8.setForeground(Color.LIGHT_GRAY);
		textField_8.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_8.setColumns(10);
		textField_8.setBounds(349, 130, 221, 45);
		panel_2.add(textField_8);
		
		textField_9 = new JTextField();
		textField_9.setText("E-Mail");
		textField_9.setForeground(Color.LIGHT_GRAY);
		textField_9.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_9.setColumns(10);
		textField_9.setBounds(632, 130, 221, 45);
		panel_2.add(textField_9);
		
		btnEkle = new JButton("Ekle");
		btnEkle.setForeground(Color.BLACK);
		btnEkle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnEkle.setBounds(938, 130, 221, 45);
		panel_2.add(btnEkle);
	}
}
