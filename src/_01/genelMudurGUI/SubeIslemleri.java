package _01.genelMudurGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import _01.DAO.SubeIslemleriDB;

import javax.swing.JTabbedPane;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SingleSelectionModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SubeIslemleri extends JFrame {

	DefaultTableModel model = new DefaultTableModel();
	DefaultTableModel model2 = new DefaultTableModel();
	DefaultTableModel model3 = new DefaultTableModel();
	SubeIslemleriDB subeIslemleri = new SubeIslemleriDB();
	Object[] satirlar = new Object[6];

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTable table_1;
	private JTextField textField_6;
	private JTable table_2;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SubeIslemleri frame = new SubeIslemleri();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SubeIslemleri() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1300, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 17));
		tabbedPane.setBounds(12, 27, 1258, 813);
		contentPane.add(tabbedPane);

		model.setRowCount(0);
		model = subeIslemleri.subeVerileri(model);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Sube Ekleme", null, panel, null);
		panel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(48, 85, 700, 570);
		panel.add(scrollPane);

		table = new JTable();
		table.setModel(model);

		scrollPane.setViewportView(table);

		textField = new JTextField();
		textField.setBounds(780, 150, 216, 47);
		panel.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(1025, 150, 216, 47);
		panel.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(780, 274, 216, 47);
		panel.add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(1025, 274, 216, 47);
		panel.add(textField_3);

		table.setModel(model);

		JButton btnNewButton = new JButton("Sube Ekle");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				model.setRowCount(0);

				subeIslemleri.subeEkleme(textField.getText(), textField_1.getText(),
						textField_2.getText(), textField_3.getText(), textField_4.getText(), textField_5.getText());
				model = subeIslemleri.subeVerileri(model);
				table.setModel(model);

				model2.setRowCount(0);
				model2 = subeIslemleri.subeVerileri(model2);
				table_1.setModel(model2);

				model3.setRowCount(0);
				model3 = subeIslemleri.subeVerileri(model3);

				table_2.setModel(model3);

			}
		});
		btnNewButton.setBounds(1025, 510, 216, 47);
		panel.add(btnNewButton);

		JLabel lblNewLabel = new JLabel("Sube Numaras\u0131n\u0131 Giriniz:");
		lblNewLabel.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(778, 102, 172, 27);
		panel.add(lblNewLabel);

		JLabel lblSubeIsminiGiriniz = new JLabel("Sube \u0130smini Giriniz:");
		lblSubeIsminiGiriniz.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		lblSubeIsminiGiriniz.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSubeIsminiGiriniz.setBounds(1025, 102, 172, 27);
		panel.add(lblSubeIsminiGiriniz);

		JLabel lblSubeAdresiniGiriniz = new JLabel("Sube Adresini Giriniz:");
		lblSubeAdresiniGiriniz.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		lblSubeAdresiniGiriniz.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSubeAdresiniGiriniz.setBounds(778, 225, 172, 27);
		panel.add(lblSubeAdresiniGiriniz);

		JLabel lblSubeTelefonNumarasn = new JLabel("Sube Telefon Numaras\u0131n\u0131 Giriniz:");
		lblSubeTelefonNumarasn.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		lblSubeTelefonNumarasn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSubeTelefonNumarasn.setBounds(1025, 225, 216, 27);
		panel.add(lblSubeTelefonNumarasn);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(780, 400, 216, 47);
		panel.add(textField_4);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(1025, 400, 216, 47);
		panel.add(textField_5);

		JLabel lblSubeEmailiniGiriniz = new JLabel("Sube E-Maili'ni Giriniz:");
		lblSubeEmailiniGiriniz.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		lblSubeEmailiniGiriniz.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSubeEmailiniGiriniz.setBounds(780, 351, 172, 27);
		panel.add(lblSubeEmailiniGiriniz);

		JLabel lblSubeKdeminiGiriniz = new JLabel("Sube K\u0131demini Giriniz:");
		lblSubeKdeminiGiriniz.setToolTipText("");
		lblSubeKdeminiGiriniz.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSubeKdeminiGiriniz.setBounds(1025, 351, 172, 27);
		panel.add(lblSubeKdeminiGiriniz);

		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Sube Guncelleme", null, panel_2, null);
		panel_2.setLayout(null);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(49, 87, 687, 597);
		panel_2.add(scrollPane_2);

		table_2 = new JTable();
		scrollPane_2.setViewportView(table_2);

		model3.setRowCount(0);
		model3 = subeIslemleri.subeVerileri(model3);

		table_2.setModel(model3);

		JLabel label_1 = new JLabel("Sube Numaras\u0131n\u0131 Giriniz:");
		label_1.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_1.setBounds(760, 129, 172, 27);
		panel_2.add(label_1);

		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(760, 188, 216, 47);
		panel_2.add(textField_7);

		JLabel label_2 = new JLabel("Sube Adresini Giriniz:");
		label_2.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_2.setBounds(760, 291, 172, 27);
		panel_2.add(label_2);

		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(760, 347, 216, 47);
		panel_2.add(textField_8);

		JLabel label_3 = new JLabel("Sube E-Maili'ni Giriniz:");
		label_3.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_3.setBounds(760, 452, 172, 27);
		panel_2.add(label_3);

		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(760, 504, 216, 47);
		panel_2.add(textField_9);

		JLabel label_4 = new JLabel("Sube \u0130smini Giriniz:");
		label_4.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_4.setBounds(1016, 129, 172, 27);
		panel_2.add(label_4);

		textField_10 = new JTextField();
		textField_10.setColumns(10);
		textField_10.setBounds(1016, 188, 216, 47);
		panel_2.add(textField_10);

		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(1016, 347, 216, 47);
		panel_2.add(textField_11);

		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(1016, 504, 216, 47);
		panel_2.add(textField_12);

		JLabel label_5 = new JLabel("Sube Telefon Numaras\u0131n\u0131 Giriniz:");
		label_5.setToolTipText("6 basamakl\u0131 say\u0131 giriniz...");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_5.setBounds(1016, 291, 216, 27);
		panel_2.add(label_5);

		JLabel label_6 = new JLabel("Sube K\u0131demini Giriniz:");
		label_6.setToolTipText("");
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_6.setBounds(1016, 452, 172, 27);
		panel_2.add(label_6);

		JButton button_1 = new JButton("Sube Ekle");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				model3.setRowCount(0);
				subeIslemleri.subeGuncelle(textField_7.getText(), textField_10.getText(), textField_8.getText(),
						textField_11.getText(), textField_9.getText(), textField_12.getText());
				model3 = subeIslemleri.subeVerileri(model3);
				table_2.setModel(model3);// sube g�ncellemde s�k�nt� var ona bak

				model.setRowCount(0);
				model = subeIslemleri.subeVerileri(model);
				table.setModel(model);

				model2.setRowCount(0);
				model2 = subeIslemleri.subeVerileri(model2);
				table_1.setModel(model2);

			}
		});
		button_1.setBounds(1016, 613, 216, 47);
		panel_2.add(button_1);

		JPanel panel_1 = new JPanel();

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(36, 101, 720, 595);
		panel_1.add(scrollPane_1);

		table_1 = new JTable();
		scrollPane_1.setViewportView(table_1);

		tabbedPane.addTab("Sube Kald�rma", null, panel_1, null);
		panel_1.setLayout(null);

		model2.setRowCount(0);
		model2 = subeIslemleri.subeVerileri(model2);
		table_1.setModel(model2);

		textField_6 = new JTextField();
		textField_6.setToolTipText(
				"Kald\u0131rmak \u0130stedi\u011Finiz Subenin 6 Basamakl\u0131 Numaras\u0131n\u0131 Giriniz...");
		textField_6.setColumns(10);
		textField_6.setBounds(779, 335, 216, 47);
		panel_1.add(textField_6);

		JButton button = new JButton("Sube Ekle");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				subeIslemleri.subeSilme(textField_6.getText());
				model2.setRowCount(0);
				model2 = subeIslemleri.subeVerileri(model2);
				table_1.setModel(model2);

				model.setRowCount(0);
				model = subeIslemleri.subeVerileri(model);
				table.setModel(model);

				model2.setRowCount(0);
				model2 = subeIslemleri.subeVerileri(model2);
				table_1.setModel(model2);

				model3.setRowCount(0);
				model3 = subeIslemleri.subeVerileri(model3);

				table_2.setModel(model3);
			}
		});
		button.setBounds(1025, 335, 216, 47);
		panel_1.add(button);

		JLabel label = new JLabel("Sube Numaras\u0131n\u0131 Giriniz:");
		label.setToolTipText("");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(782, 272, 172, 27);
		panel_1.add(label);
	}
}
